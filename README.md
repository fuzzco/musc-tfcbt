# Getting up and running

## Prerequisites

You'll need the following already installed:

 * ruby 2.0.0-p247 (installed via rbenv, which is in turn installed via homebrew)
 * postgresql (installed via homebrew)

## Postrequisites

Given the above, you "should" be able to do the following:

	bundle install
	rake db:create
	rake db:migrate
	rake db:seed
	rails s
 
There are sample admin and provider accounts you can use; their username/passwords can be found in `seeds.rb`.
 
# Adding new content

In order to add new chapters, you'll need to:
 * Update seeds.rb to insert new rows into the 'chapters' and 'sections' table representing your new chapter and its exercises
 * Add html.erb files for each of the chapter's exercises to /app/views/sections/
  * Update sections_controller.rb::show to translate from a chapter/section sequence to your html.erb files
 * Update provider_controller.rb::export to include appropriate columns for your chapter 