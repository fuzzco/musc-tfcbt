source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0'

# Use sqlite3 as the database for Active Record
group :test do
  gem 'sqlite3'
end

# Use postgresql
group :development, :production do
  gem 'pg'
end

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

gem 'jquery-ui-rails'

# jQuery Touch Punch
# https://github.com/geothird/touchpunch-rails
gem 'touchpunch-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Fix bind problems introduced by turbolinks
# https://github.com/kossnocorp/jquery.turbolinks
gem 'jquery-turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0'

gem 'devise'

gem 'deep_cloneable'

# Location support
# (Carmen)[https://github.com/jim/carmen-rails]
# Pointing directly due to this issue:
# http://stackoverflow.com/questions/18518850/carmen-rails-country-select-wrong-number-of-arguments-3-for-0
gem 'carmen-rails', '~> 1.0.0', github: 'jim/carmen-rails'

# (Rails 12factor)[https://github.com/heroku/rails_12factor]
# Required for deploying to Heroku
gem 'rails_12factor', group: :production

ruby "2.0.0"
# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]