MuscTfcbt::Application.routes.draw do

  get "home/index"
  root :to => "home#index"

  devise_for :providers

  get '/providers/csv' => 'providers#export', as: 'export_providers'

  get '/providers/:id/prepare' => 'providers#prepare', as: 'prepare_providers'
  
  resources :providers do
    resources :patients, only: [:new, :create]
  end
  
  resources :patients, only: [:show, :edit, :update] do
    resources :assignments, only: [:index]
  end
  
  resources :decks, only: [:index, :edit, :update]
  resources :section_questions, only: [:edit, :update]

  put 'patients/:id/assign/:homework_id' => 'patients#assign_homework', as: 'assign_patient_homework'
  put 'patients/:id/deactivate' => 'patients#deactivate', as: 'deactivate_patient'
  put 'patients/:id/activate' => 'patients#activate', as: 'activate_patient'
  put 'patients/:id/start' => 'patients#start_session', as: 'start_patient_session'
  
  resources :chapters, only: [:index, :edit, :show, :update] do
    resources :sections, only: [:show, :create, :destroy]
  end
  
  resources :prepares, only: [:show]
  
  put 'chapters/:chapter_id/sections/:id/finish' => 'sections#finish', as: 'finish_chapter_section'
  put 'chapters/:chapter_id/sections/:id/interaction' => 'sections#interaction', as: 'interaction_chapter_section'
  
  get 'content/edit' => 'content#edit', as: 'edit_content'
  post 'content/edit' => 'content#update_audio_url', as: 'update_audio_url'
  patch 'content' => 'content#update'
  
  get 'homework_category/:id/list' => 'homework_category#list', as: 'list_homework_category'

end
