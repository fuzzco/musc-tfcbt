class RemovePasswordDigestFromProviders < ActiveRecord::Migration
  def change
    remove_column :providers, :password_digest
  end
end
