class AddBodyClassToDecks < ActiveRecord::Migration
  def change
    add_column :decks, :body_class, :string
  end
end
