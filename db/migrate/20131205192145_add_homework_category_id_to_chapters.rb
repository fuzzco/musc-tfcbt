class AddHomeworkCategoryIdToChapters < ActiveRecord::Migration
  def change
    add_column :chapters, :homework_category_id, :integer
  end
end
