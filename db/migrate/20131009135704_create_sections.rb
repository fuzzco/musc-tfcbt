class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.integer :chapter_id
      t.integer :sequence

      t.timestamps
    end
  end
end
