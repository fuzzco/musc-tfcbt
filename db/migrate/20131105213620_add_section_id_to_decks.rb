class AddSectionIdToDecks < ActiveRecord::Migration
  def change
    add_column :decks, :section_id, :integer
  end
end
