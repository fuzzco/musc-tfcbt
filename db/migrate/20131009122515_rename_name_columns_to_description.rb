class RenameNameColumnsToDescription < ActiveRecord::Migration
  def change
    rename_column :degrees, :name, :description
    rename_column :professions, :name, :description
    rename_column :traumas, :name, :description
  end
end
