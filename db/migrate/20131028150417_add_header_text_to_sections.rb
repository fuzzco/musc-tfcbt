class AddHeaderTextToSections < ActiveRecord::Migration
  def change
    add_column :sections, :header_text, :string
  end
end
