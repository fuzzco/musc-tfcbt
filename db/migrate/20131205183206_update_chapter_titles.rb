class UpdateChapterTitles < ActiveRecord::Migration
  def change
    chapter_names = {
      1 => 'What Do You Know?',
      2 => 'You Are Not Alone',
      3 => 'Your Body',
      4 => 'Parenting',
      5 => 'Relaxation: Breathing',
      6 => 'Relaxation: PMR',
      7 => 'Affective Regulation',
      8 => 'Cognitive Coping',
      9 => 'Trauma Narrative',
      10 => 'In Vivo Mastery',
      11 => 'Enhancing Safety'
    }
    chapter_names.each do |display_order, name|
      Chapter.where(display_order: display_order).update_all(name: name)
    end
  end
end
