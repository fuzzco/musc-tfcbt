class FlattenTraumaToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :trauma1_id, :integer
    add_column :patients, :trauma2_id, :integer
    add_column :patients, :trauma3_id, :integer
    add_column :patients, :trauma4_id, :integer
  end
end
