class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.integer :provider_id
      t.string :lookup_id

      t.timestamps
    end
  end
end
