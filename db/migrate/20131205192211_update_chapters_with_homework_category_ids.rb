class UpdateChaptersWithHomeworkCategoryIds < ActiveRecord::Migration
  def change
    chapter_homework_category_ids = {
      11  => 2,   #parenting            => parenting
      1   => 1,   #your body            => psychoeduation
      4   => 1,   #you are not alone    => psychoeducation
      5   => 1,   #what do you know     => psychoeducation
      2   => 3,   #relax: breathing     => relaxation
      3   => 3,   #relax: pmr           => relaxation
      6   => 4,   #affective regulation => affective regulation
      7   => 5,   #cognitive coping     => cognitive coping
      10  => 6,   #trauma narrative     => trauma narrative
      9   => 7,   #in-vivo exposure     => in vivo exposure
      8   => 9    #enhancing safety     => enhancing safety
    }
    chapter_homework_category_ids.each do |chapter_id, homework_category_id|
      Chapter.where(id: chapter_id).update_all(homework_category_id: homework_category_id)
    end
  end
end
