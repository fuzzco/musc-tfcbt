class AddProviderToDeck < ActiveRecord::Migration
  def change
    add_column :decks, :provider_id, :integer
    add_column :decks, :original, :boolean, :default => false
  end
end
