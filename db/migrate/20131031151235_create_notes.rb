class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :section_id
      t.integer :card_id
      t.string :content

      t.timestamps
    end
  end
end
