class AddIsActiveToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :is_active, :boolean, default: true
  end
end
