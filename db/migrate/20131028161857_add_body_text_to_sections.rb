class AddBodyTextToSections < ActiveRecord::Migration
  def change
    add_column :sections, :body_text, :string
  end
end
