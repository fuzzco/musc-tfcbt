class CreatePatientTraumas < ActiveRecord::Migration
  def change
    create_table :patient_traumas do |t|
      t.integer :patient_id
      t.integer :trauma_id
      t.integer :sequence

      t.timestamps
    end
  end
end
