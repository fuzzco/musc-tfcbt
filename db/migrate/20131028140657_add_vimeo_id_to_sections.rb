class AddVimeoIdToSections < ActiveRecord::Migration
  def change
    add_column :sections, :vimeo_id, :string
  end
end
