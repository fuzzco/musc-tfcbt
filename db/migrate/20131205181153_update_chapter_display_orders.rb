class UpdateChapterDisplayOrders < ActiveRecord::Migration
  def change
    chapter_display_order_map = {
      1 => 3, 
      2 => 5, 
      3 => 6, 
      4 => 2, 
      5 => 1, 
      6 => 7, 
      7 => 8, 
      8 => 11, 
      9 => 10, 
      10 => 9, 
      11 => 4
    }
    chapter_display_order_map.each do |chapter_id, display_order|
      Chapter.where(id: chapter_id).update_all(display_order: display_order)
    end
  end
end
