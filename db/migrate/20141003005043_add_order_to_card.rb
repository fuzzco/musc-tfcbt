class AddOrderToCard < ActiveRecord::Migration
  def change
    add_column :cards, :card_order, :integer
    add_column :cards, :active, :boolean, :default => true
  end
end
