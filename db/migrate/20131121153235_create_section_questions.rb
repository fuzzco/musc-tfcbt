class CreateSectionQuestions < ActiveRecord::Migration
  def change
    create_table :section_questions do |t|
      t.references :section, index: true
      t.integer :sequence
      t.string :question
      t.integer :answer

      t.timestamps
    end
  end
end
