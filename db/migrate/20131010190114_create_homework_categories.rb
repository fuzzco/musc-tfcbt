class CreateHomeworkCategories < ActiveRecord::Migration
  def change
    create_table :homework_categories do |t|
      t.string :description

      t.timestamps
    end
  end
end
