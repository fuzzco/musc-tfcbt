class AddVimeoToCard < ActiveRecord::Migration
  def change
    add_column :cards, :vimeo_url, :string
  end
end
