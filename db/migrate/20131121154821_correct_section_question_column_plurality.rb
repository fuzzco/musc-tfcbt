class CorrectSectionQuestionColumnPlurality < ActiveRecord::Migration
  def change
    add_column :section_questions, :answer_boys, :integer
    add_column :section_questions, :answer_girls, :integer
    remove_column :section_questions, :answer_boy
    remove_column :section_questions, :answer_girl
  end
end
