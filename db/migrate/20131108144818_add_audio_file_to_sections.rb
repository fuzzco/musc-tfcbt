class AddAudioFileToSections < ActiveRecord::Migration
  def change
    add_column :sections, :audio_file, :string
  end
end
