class AddDataKeyToInteractions < ActiveRecord::Migration
  def change
    add_column :interactions, :data_key, :string
  end
end
