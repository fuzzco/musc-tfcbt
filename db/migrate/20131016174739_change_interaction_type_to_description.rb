class ChangeInteractionTypeToDescription < ActiveRecord::Migration
  def change
    remove_column :interactions, :type
    add_column :interactions, :description, :string
  end
end
