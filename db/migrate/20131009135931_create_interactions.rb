class CreateInteractions < ActiveRecord::Migration
  def change
    create_table :interactions do |t|
      t.integer :section_id
      t.integer :patient_id
      t.string :type
      t.string :data

      t.timestamps
    end
  end
end
