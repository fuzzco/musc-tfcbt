class AddRaceEthnicityGenderToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :race_id, :integer
    add_column :patients, :ethnicity_id, :integer
    add_column :patients, :gender_id, :integer
  end
end
