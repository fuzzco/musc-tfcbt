class UpdateQuestionsWithGenderAnswers < ActiveRecord::Migration
  def change
    add_column :section_questions, :answer_boy, :integer
    add_column :section_questions, :answer_girl, :integer
    remove_column :section_questions, :answer
  end
end
