class AddVimeoUrlToSections < ActiveRecord::Migration
  def change
    add_column :sections, :vimeo_url, :string
    remove_column :sections, :vimeo_id
  end
end
