class CreateTraumas < ActiveRecord::Migration
  def change
    create_table :traumas do |t|
      t.string :name

      t.timestamps
    end
  end
end
