class AddStateCodeToProviders < ActiveRecord::Migration
  def change
    add_column :providers, :state_code, :string
  end
end
