class AddCountryCodeToProviders < ActiveRecord::Migration
  def change
    add_column :providers, :country_code, :string
  end
end
