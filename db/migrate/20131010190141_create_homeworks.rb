class CreateHomeworks < ActiveRecord::Migration
  def change
    create_table :homeworks do |t|
      t.string :description
      t.integer :homework_category_id

      t.timestamps
    end
  end
end
