class AddPrepareVimeoUrlToChapters < ActiveRecord::Migration
  def change
    add_column :chapters, :prepare_vimeo_url, :string
  end
end
