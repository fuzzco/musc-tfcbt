class AddNewCognitiveCopingSections < ActiveRecord::Migration
  def change
    
    #insert three new sections in the middle of the chapter
    Section.where(chapter_id: 7).where("sequence >= 9").order("sequence DESC").each do |s|
      s.sequence = s.sequence + 3
      s.save
    end
    
    Section.create(chapter_id: 7, sequence: 9, header_text: "Let's see how the child was feeling.")
    Section.create(chapter_id: 7, sequence: 10, header_text: "If the child had the thought, **FEELING**, what do you think the child might be feeling?")
    Section.create(chapter_id: 7, sequence: 11, header_text: "Let's see how the child was feeling.")
  end
end
