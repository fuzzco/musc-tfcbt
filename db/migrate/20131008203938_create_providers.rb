class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.integer :degree_id
      t.integer :profession_id
      t.integer :experience_id
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
