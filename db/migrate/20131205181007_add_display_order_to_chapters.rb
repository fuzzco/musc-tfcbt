class AddDisplayOrderToChapters < ActiveRecord::Migration
  def change
    add_column :chapters, :display_order, :integer
  end
end
