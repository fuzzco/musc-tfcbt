class AddIndexToPatientTraumasSequence < ActiveRecord::Migration
  def change
    add_index :patient_traumas, [:patient_id, :sequence], :unique => true
  end
end
