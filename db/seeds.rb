# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Set the initial base audio url- editiable in /content/edit and content_controller.
Note.create(content:"http://dev.fuzzco.com/musctfcbt.com/-audio/")

Degree.create(
  [
    {description: 'Pre-masters'},
    {description: 'Masters'},
    {description: 'Doctorate'},
    {description: 'Medical degree'}
  ]
)

Profession.create(
  [
    {description: 'Psychology'},
    {description: 'Social work'},
    {description: 'Counseling'},
    {description: 'Marriage/family'},
    {description: 'Nursing'},
    {description: 'Psychiatry'},
    {description: 'Other'}
  ]
)

Experience.create(
  [
    {description: '<5 years'},
    {description: '5-10 years'},
    {description: '10-20 years'},
    {description: '20+ years'}
  ]
)

Race.create(
  [
    {description: 'White'},
    {description: 'Black/African American'},
    {description: 'Asian'},
    {description: 'Native American'},
    {description: 'Native Hawaiin/Pacific Islander'},
    {description: 'Other'}
  ]
)

Ethnicity.create(
  [
    {description: 'Hispanic'},
    {description: 'Non-Hispanic'}
  ]
)

Trauma.create(
  [
    {description: 'Sexual abuse/assault'},
    {description: 'Physical abuse/assault'},
    {description: 'Accident/injury'},
    {description: 'Domestic violence'},
    {description: 'School/community violence'},
    {description: 'Disaster or mass violence'},
    {description: 'Bullying'},
    {description: 'Death of a loved one'},
    {description: 'Other'}
  ]
)

Gender.create(
  [
    {id: 1, description: 'Male'},
    {id: 2, description: 'Female'}
  ]
)


HomeworkCategory.create(
  [
    {
      description: 'Psychoeducation',
      homeworks: Homework.create([
        {description: 'Family review of educational handouts'}
      ])
    },
    {
      description: 'Parenting',
      homeworks: Homework.create([
        {description: 'Practice positive reinforcement, praise'},
        {description: 'Practice giving effective instructions'},
        {description: 'Practice using time-out'},
        {description: 'Practice contingency management skills'},
        {description: 'Complete a daily diary of behavior/symptoms'}
      ])
    },
    {
      description: 'Relaxation',
      homeworks: Homework.create([
        {description: 'Generate a list of relaxing activities'},
        {description: 'Do some aerobic activity or exercise'},
        {description: 'Practice deep breathing'},
        {description: 'Practice progressive muscle relaxation'},
        {description: 'Practice mindfulness by focusing on one thing'}
      ])
    },
    {
      description: 'Affective Regulation',
      homeworks: Homework.create([
        {description: 'Play the charades game'},
        {description: 'Practice positive self-talk'},
        {description: 'Practice problem-solving and social skills'},
        {description: 'Play the wheel of feelings at home'}
      ])
    },
    {
      description: 'Cognitive Coping',
      homeworks: Homework.create([
        {description: 'Practice identifying and labeling thoughts, feelings, behavior'},
        {description: 'Thought record to track dysfunctional thoughts when upset'},
        {description: 'Write down all thoughts about one thing that happened'}
      ])
    },
    {
      description: 'Trauma Narrative',
      homeworks: Homework.create([
        {description: 'Practice one of the new ways to think about what happened'},
        {description: 'Identify cognitive or thought distortions throughout the week'}
      ])
    },
    {
      description: 'In Vivo Exposure',
      homeworks: Homework.create([
        {description: 'Complete activities on the in-vivo list (parent help, as needed)'}
      ])
      
    },
    {
      description: 'Conjoint Sessions',
      homeworks: Homework.create([
        {description: 'Think about questions/concerns to address in conjoin sessions'},
        {description: 'Plan a fun caregiver-child activity'},
        {description: 'Have the caregiver and child practice communication skill building'}
      ])
    },
    {
      description: 'Enhancing Safety',
      homeworks: Homework.create([
        {description: 'Educate others about the safety plan'},
        {description: 'Role play the safety plan with others'},
        {description: 'Role play and practice verbal and non-verbal communication'}
      ])
    }
  ]
)

Chapter.create(
  [
    {id: 1, display_order: 3, homework_category_id: 1, name: 'Your Body'},
    {id: 2, display_order: 5, homework_category_id: 3, name: 'Relaxation: Breathing'},
    {id: 3, display_order: 6, homework_category_id: 3, name: 'Relaxation: PMR'},
    {id: 4, display_order: 2, homework_category_id: 1, name: 'You Are Not Alone'},
    {id: 5, display_order: 1, homework_category_id: 1, name: 'What Do You Know?'},
    {id: 6, display_order: 7, homework_category_id: 4, name: 'Affective Regulation'},
    {id: 7, display_order: 8, homework_category_id: 5,name: 'Cognitive Coping'},
    {id: 8, display_order: 11, homework_category_id: 9, name: 'Enhancing Safety'},
    {id: 9, display_order: 10, homework_category_id: 7, name: 'In Vivo Exposure'},
    {id: 10, display_order: 9, homework_category_id: 6, name: 'Trauma Narrative'},
    {id: 11, display_order: 4, homework_category_id: 2, name: 'Parenting'}
  ]
)

Section.create(
  [
    {chapter_id: 1, sequence: 1, content_type: 'video'}, #1
    {chapter_id: 1, sequence: 2, header_text: 'Drag the names of the body parts to the right place.'},
    {chapter_id: 1, sequence: 3, header_text: 'Drag the names of the body parts to the right place.'},
    {chapter_id: 1, sequence: 4, header_text: 'Drag the names of the body parts to the right place.'},
    {chapter_id: 1, sequence: 5, header_text: 'Drag the names of the body parts to the right place.'},
    {chapter_id: 2, sequence: 1, content_type: 'video'},
    {chapter_id: 2, sequence: 2, audio_file: 'breathing/breathing_april1.mp3'},
    {chapter_id: 2, sequence: 3, audio_file: 'breathing/breathing_april2.mp3'},
    {chapter_id: 3, sequence: 1, content_type: 'video'},
    {chapter_id: 3, sequence: 2, header_text: 'Choose a muscle group.'},
    {chapter_id: 4, sequence: 1, content_type: 'video'},
    {chapter_id: 4, sequence: 2, header_text: 'How many of these $GENDER_PRONOUN_PLURAL...'}, #10
    {chapter_id: 4, sequence: 3, header_text: 'How many of these $GENDER_PRONOUN_PLURAL...'}, #10
    {chapter_id: 5, sequence: 1, content_type: 'video'},
    {chapter_id: 5, sequence: 2, header_text: 'Choose a deck.'},
    {chapter_id: 5, sequence: 3, header_text: 'Swipe through the cards.'},
    {chapter_id: 6, sequence: 1, content_type: 'video'},
    {chapter_id: 6, sequence: 2, header_text: 'What are some words that describe how people FEEL? List as many as you can in 90 seconds.'},
    {chapter_id: 6, sequence: 3, header_text: 'Try it: Spin the Wheel.'},
    {chapter_id: 6, sequence: 4, header_text: 'Feelings Charades: Spin the wheel, and act out the feeling!'},
    {chapter_id: 6, sequence: 5, header_text: 'When we have an upsetting feeling, there are things we can do to feel better. Pick an upsetting feeling. What are some things YOU can do to feel better when you have that feeling? You can write your answer, or draw it.'},
    {chapter_id: 6, sequence: 6, header_text: 'Write down all of the feelings you experienced at the time of your traumatic event. List as many as you can in 90 seconds.'},
    {chapter_id: 7, sequence: 1, content_type: 'video'}, #20
    {chapter_id: 7, sequence: 2, header_text: 'Let\'s review what is a feeling, a thought and an action. Drag the feelings, actions, and thoughts to the right place.'},
    {chapter_id: 7, sequence: 3, header_text: 'Let\'s review what is a feeling, a thought and an action. Drag the feelings, actions, and thoughts to the right place.'},
    {chapter_id: 7, sequence: 4, header_text: 'Watch the video and then go to the next screen to answer a question about what the child in the video is thinking.', vimeo_url:"98069146"},
    {chapter_id: 7, sequence: 5, header_text: 'What might the child in the video be thinking? <br/>Choose the best answer.'},
    {chapter_id: 7, sequence: 6, header_text: 'If the child had the thought, **FEELING**, what do you think the child might be feeling?'},
    {chapter_id: 7, sequence: 7, header_text: 'Let’s see how the child was feeling.'},
    {chapter_id: 7, sequence: 8, header_text: 'If the child had the thought, **FEELING**, what do you think the child might be feeling?'},
    {chapter_id: 7, sequence: 9, header_text: "Let's see how the child was feeling."},
    {chapter_id: 7, sequence: 10, header_text: "If the child had the thought, **FEELING**, what do you think the child might be feeling?"},
    {chapter_id: 7, sequence: 11, header_text: "Let's see how the child was feeling."},
    {chapter_id: 7, sequence: 12, header_text: 'Let\'s talk about what happens to your feelings and actions when you change your thoughts.', body_text: 'Carla got a bad score on a test.'},
    {chapter_id: 7, sequence: 13, header_text: 'Carla thinks her score is bad because her teacher is being unfair.'},
    {chapter_id: 7, sequence: 14, header_text: 'How do you think Carla feels? You can choose more than one answer.'},
    {chapter_id: 7, sequence: 15, header_text: 'What will Carla do?'}, #30
    {chapter_id: 7, sequence: 16, header_text: 'Carla got a bad score on a test.'},
    {chapter_id: 7, sequence: 17, header_text: 'Carla thinks "I am stupid."'},
    {chapter_id: 7, sequence: 18, header_text: 'What feeling goes with Carla\'s thought? You can choose more than one answer.'},
    {chapter_id: 7, sequence: 19, header_text: 'What will Carla do?'},
    {chapter_id: 7, sequence: 20, header_text: 'Carla got a bad score on a test.'},
    {chapter_id: 7, sequence: 21, header_text: 'Carla thinks "I can try harder next time."'},
    {chapter_id: 7, sequence: 22, header_text: 'What feeling goes with Carla\'s thought? You can choose more than one answer.'},
    {chapter_id: 7, sequence: 23, header_text: 'What will Carla do?'},
    {chapter_id: 7, sequence: 24, header_text: 'Discuss how Carla\'s feelings and actions changed with your provider.'},
    {chapter_id: 7, sequence: 25, header_text: 'Now let\'s examine your thoughts, feelings, and actions.', body_text: 'Describe a situation that happened to you in the past month.'}, #40
    {chapter_id: 7, sequence: 26, header_text: 'Now describe the thought you had in this situation.'},
    {chapter_id: 7, sequence: 27, header_text: 'When you had this thought, how did you feel?'},
    {chapter_id: 7, sequence: 28, header_text: 'What did you do when you had these feelings?'},
    {chapter_id: 7, sequence: 29, header_text: 'Now, change your thought to something different.', body_text: 'How would that have affected your feelings? How would that have affected your actions?'},
    {chapter_id: 8, sequence: 1, content_type: 'video'},
    {chapter_id: 8, sequence: 2, header_text: 'Choose a deck.'},
    {chapter_id: 8, sequence: 3, header_text: 'Swipe through the cards.'},
    {chapter_id: 9, sequence: 1, content_type: 'video'},
    {chapter_id: 9, sequence: 2, audio_file: 'beginning_narration.mp3'},
    {chapter_id: 9, sequence: 3, audio_file: '3_narration.mp3'}, #50
    {chapter_id: 9, sequence: 4},
    {chapter_id: 9, sequence: 5},
    {chapter_id: 9, sequence: 6},
    {chapter_id: 9, sequence: 7},
    {chapter_id: 9, sequence: 8},
    {chapter_id: 9, sequence: 9},
    {chapter_id: 9, sequence: 10, audio_file: '5a_narration.mp3'},
    {chapter_id: 9, sequence: 11, audio_file: '5b_narration.mp3'},
    {chapter_id: 9, sequence: 12, audio_file: '5c_narration.mp3'},
    {chapter_id: 9, sequence: 13, audio_file: '5d_narration.mp3'},
    {chapter_id: 9, sequence: 14, audio_file: '6_narration.mp3'},
    {chapter_id: 9, sequence: 15, audio_file: 'final_narration.mp3'},
    {chapter_id: 10, sequence: 1, content_type: 'video'},
    {chapter_id: 10, sequence: 2, header_text: 'Now work with your provider to create trauma narratives in the Paper application. Be sure to save your work to review later.'},
    {chapter_id: 11, sequence: 1, header_text: 'Choose a topic.'},
    {chapter_id: 11, sequence: 2, header_text: 'Swipe through the videos.'},
  ]
)

Deck.create(
  [
    # chapter 5 decks
    {title: "General Questions", body_class: "green"},   #deck 1
    {title: "Domestic Violence", body_class: "blue"},
    {title: "Sexual Abuse", body_class: "orange"},
    {title: "Physical Abuse", body_class: "red"},
    {title: "Personal Safety", body_class: "red"},     #deck 5
    {title: "Disasters", body_class: "orange"},
    {title: "Serious Accidents", body_class: "blue"},
    {title: "Bullying/Peer Victimization", body_class: "green"},
    # chapter 8 decks
    {title: "OK/Not-OK Touches", body_class: "green"},
    {title: "Substance Abuse", body_class: "blue"},       #deck 10
    {title: "Peer Victimization", body_class: "orange"},
    {title: "Witnessed Violence", body_class: "red"},
    {title: "Internet Safety", body_class: "green"},
    {title: "Cyberbullying", body_class: "blue"},
    #chapter 11 video decks
    {title: 'Praise',body_class: "green"},
    {title: 'Contingency',body_class: "green"},
    {title: 'Removal of Privileges',body_class: "green"},
    {title: 'Active Ignoring',body_class: "green"},
    {title: 'ABC Model',body_class: "green"}
  ]
)

c5s2 = Section.where(chapter_id: 5, sequence: 2).first
c5s2.decks << Deck.order("id ASC").take(8)
c5s2.save

c8s2 = Section.where(chapter_id: 8, sequence: 2).first
c8s2.decks << Deck.order("id ASC").limit(6).offset(8)
c8s2.save

c11s1 = Section.where(chapter_id: 11, sequence: 1).first
c11s1.decks << Deck.order("id ASC").limit(5).offset(14)
c11s1.save

#general
deck = Deck.find(1)
deck.cards.create(
  [
		{question: "Tell about a time when you felt proud."},
		{question: "Describe something the person taking care of you does that makes you feel good."},
		{question: "In order to solve a problem or conflict, is it best to be passive, assertive, or aggressive?"},
		{question: "How does someone look and sound when they are being assertive? How does someone look and sound when they are being aggressive? How does someone look and sound when they are being passive?"},
		{question: "How do you think a family's faith could help them in dealing with a problem of abuse or violence?"},
		{question: "Feel proud of yourself for playing this game. Pat yourself on the back!"},
		{question: "What are some healthy ways of resolving problems or conflicts?"},
		{question: "What does \"love\" mean? How can you tell if someone loves you?"},
		{question: "Who can you talk to about a problem?"},
		{question: "Name 3 things you like about your family."},
		{question: "Name 3 ways that you can calm yourself down when you're feeling angry or upset."},
		{question: "Is it okay for people to get angry? What are some healthy ways people express their anger? What are some unhealthy ways people express their anger?"},
		{question: "What is the best way to know how someone is feeling?"},
		{question: "Name 3 things you like about yourself."},
    {question: "What do you like most about counseling?"}
  ]
)

#domestic violence
deck = Deck.find(2)
deck.cards.create(
  [
		{question: "How can you tell if domestic violence happens in a child's home or family?"},
		{question: "Who should a child talk to about fighting in his/her home or family?"},
		{question: "Children can't usually stop domestic violence from happening, but telling someone who can help is important. Who could a child tell if he or she needed help?"},
		{question: "How might a child feel when domestic violence happens?"},
		{question: "Is it ever the child's fault if grown-ups in a family are physically fighting?"},
		{question: "If a child tells a grown-up about domestic violence, and they don't understand or help, what should the child do?"},
		{question: "If adults are physically fighting, what should a child do?"},
		{question: "What is domestic violence? Give an example."},
		{question: "Is it okay for grown-ups to fight? How?"},
		{question: "How might a child act when domestic violence happens?"},
		{question: "Why don't children tell about domestic violence?"},
    {question: "Why does domestic violence happen?"}
  ]
)

#sexual abuse
deck = Deck.find(3)
deck.cards.create(
  [
		{question: "What are the doctor's names for a boy's private parts?"},
		{question: "What are the doctor's names for a girl's private parts?"},
		{question: "What would you tell other children about child sexual abuse?"},
		{question: "When is it okay for someone to touch a child's private parts?"},
		{question: "How do children feel when they have been sexually abused?"},
		{question: "How do children deal with being sexually abused?"},
    {question: "How can you tell if a child has been sexually abused?"},
		{question: "Whose fault is it when a child has been sexually abused? Is it ever the child's fault?"},
		{question: "Children can't usually stop child sexual abuse from happening, but telling someone who can help is important. Who could a child tell if he/she needed help?"},
		{question: "Why does child sexual abuse happen?"},
		{question: "What might children worry about if they have been sexually abused?"},
		{question: "Why don't children tell about child sexual abuse?"},
		{question: "What is child sexual abuse? Give an example."},
		{question: "Is it okay for a child to touch his or her own private parts in private?"},
		{question: "What is the difference between sex and child sexual abuse?"}
  ]
)

#physical abuse
deck = Deck.find(4)
deck.cards.create(
  [
    {question: "How might children act when they are physically punished?"},
		{question: "How do parents feel when they use physical punishment on a child?"},
		{question: "Why do some parents physically hurt their children?"},
		{question: "Who is to blame when a parent physically hurts a child?"},
		{question: "How do you know when someone is angry? Sad? Happy?"},
		{question: "Who is physically abused?"},
		{question: "Who could a child tell if the child's parent or anyone else physically hurts him/her?"},
		{question: "What is child physical abuse? Give an example."},
		{question: "How do children feel when they have been physically abused?"},
		{question: "What are some positive ways parents/caregivers can discipline children?"},
		{question: "How do children feel when grown-ups call them names?"},
		{question: "What makes it hard for a child to tell others about child physical abuse?"},
    {question: "What is the difference between physical abuse and spanking?"}
  ]
)

#personal safety
deck = Deck.find(5)
deck.cards.create(
  [
    {question: "What are some not okay touches?"},
		{question: "What are some okay touches? Name those you like."},
		{question: "How do you feel when you talk about not okay touches? "},
		{question: "If someone tells you to keep a secret about a not okay touch and says, \"never tell,\" is that an okay kind of secret? "},
		{question: "You have the right to say \"no!\" if you are being touched in a not okay way. Practice saying \"no!\" like you really mean it."},
		{question: "What can you and your family do to stay safe?"},
		{question: "If you tell a grown-up about not okay touches, and they don't believe you, understand you, or help you, what should you do? "},
		{question: "How do you feel when you get an okay touch?"},
		{question: "Sometimes it's hard to tell if a touch is okay or not okay, and that can be confusing. What can you do if someone gives you a confusing touch? "},
		{question: "What could you do if anyone touches you with a not okay touch? "},
		{question: "What should you do if you are thinking about giving someone else a not okay touch? "},
		{question: "Practice calling 911 and asking for help. Unplug the phone first!"},
    {question: "How do children feel when grown-ups praise them?"}
  ]
)

#disasters and traumatic grief
deck = Deck.find(6)
deck.cards.create(
  [
		{question: "Children can't stop a disaster from happening, but finding someone to talk to about what happened is important. Who could a child talk to about the disaster?"},
		{question: "How might a child feel when a disaster happens?"},
		{question: "Sometimes, after a disaster happens, adults get angry and fight a lot. Is it ok for grown-ups to fight?  "},
		{question: "Why do natural disasters happen, like earthquakes, tornadoes, hurricanes, and floods? "},
		{question: "How might children act when they've been through a disaster?"},
		{question: "How do you know when your parent/caregiver is upset about what happened?"},
		{question: "How might a child feel after someone important dies suddenly, unexpectedly, or in a frightening way? "},
		{question: "After an important person dies suddenly, what are some things grown-ups might do that are confusing to young people?"},
    {question: "How could grown-ups help children after an important person died suddenly?"}
  ]
)

#serious accidents
deck = Deck.find(7)
deck.cards.create(
  [
		{question: "Who could a child talk to about a bad accident?"},
		{question: "How might a child feel when a bad accident happens?"},
		{question: "Sometimes, after something like a bad accident happens, adults get angry and fight a lot. Is it ok for grown-ups to fight?"},
		{question: "Who should a child talk to about fighting in his/her home or family?"},
		{question: "Why do accidents happen? "},
		{question: "How might children act when they've experienced a bad accident?"},
    {question: "How do you know when your parent/caregiver is upset about what happened?"}
  ]
)

#bullying/peer victimization
deck = Deck.find(8)
deck.cards.create(
  [
		{question: "How might children act when they are bullied?"},
		{question: "Who could a child talk to about being bullied? "},
		{question: "How do children feel when they've been bullied?"},
		{question: "Whose fault is it if a child has been bullied? "},
		{question: "Does bullying happen to a lot of children?  "},
		{question: "Why does bullying happen? "},
		{question: "What might children worry about if they have been bullied?"},
		{question: "How might a child feel about the person who bullies him or her?"},
		{question: "Why don't children tell about bullying?"},
    {question: "What is bullying? Give an example."} 
  ]
)

#chapter 8
#ok/not ok touches
deck = Deck.find(9)
deck.cards.create(
  [
    {question: "What are some examples of \"okay\" touches?"},
    {question: "What are some examples of \"not okay\" touches?"},
    {question: "An adult you know well touches your private area"},
    {question: "A doctor asks you to take off your clothes"},
    {question: "A teacher touches you on the shoulder or gives you a hug that feels too long"},
    {question: "Your older brother tickles you in the stomach"},
    {question: "Someone shows you their private area"},
    {question: "An adult asks you to touch their private area"},
    {question: "An adult that you know hits you"},
    {question: "You and your friends are wrestling around"},
    {question: "Someone you know offers you a ride, but you have not confirmed this plan with your caregiver"}
  ]
)

#substance abuse
deck = Deck.find(10)
deck.cards.create(
  [
    {question: "Someone hands you an alcoholic drink"},
    {question: "Someone asks if you want to smoke marijuana"},
    {question: "Your friend has been drinking alcohol and wants to drive home"},
    {question: "You have been drinking alcohol and someone you don't know offers to drive you home"},
    {question: "Your friend invites you to a party with older kids you don't know"},
    {question: "Someone you don't know comes up to you at a party and hands you a drink"},
    {question: "You are feeling bad and a friend offers you a pill that was prescribed to his mother"}
  ]
)

#peer victimization
deck = Deck.find(11)
deck.cards.create(
  [
    {question: "Someone at your school picks on you a lot and calls you names"},
    {question: "Someone at your school pushed you against the wall"},
    {question: "You see someone at your school push your friend down to the floor"},
    {question: "You go into the bathroom and see something bad written about you on the wall"},
    {question: "You are with your friends and they start saying mean things about someone else"},
    {question: "Someone you don't know asks you if you need a ride home from school"},
    {question: "Someone you don't feel comfortable around asks you to hang out"}
  ]
)

#witnessed violence
deck = Deck.find(12)
deck.cards.create(
  [
    {question: "You see someone in your neighborhood getting beat up on the street"},
    {question: "Your parents are arguing and you see one of them hit the other"},
    {question: "You hear your parents arguing and their voices are getting louder"},
    {question: "You are walking around your neighborhood and see people yelling at each other"},
    {question: "Your parent seems drunk or high"},
    {question: "You are walking home from school and you see someone on the street carrying a gun"},
    {question: "One of your classmates brings a weapon to school and shows it to you"},
    {question: "You hear gunshots"}
  ]
)

#internet safety
deck = Deck.find(13)
deck.cards.create(
  [
    {question: "You are meeting a new person online and they ask where you live"},
    {question: "You receive an e-mail or text that makes you uncomfortable"},
    {question: "A friend asks you for your e-mail or Facebook password"},
    {question: "A person you met online wants to meet you in person"},
    {question: "Someone you know sends you pictures that make you feel uncomfortable"},
    {question: "Someone asks you to send them a picture of yourself"},
    {question: "Someone asks you to send them a picture of yourself undressed"},
    {question: "A friend tells you about a new website where you can meet people"}
  ]
)

#cyber bullying
deck = Deck.find(14)
deck.cards.create(
  [
    {question: "Someone sends you threatening messages or posts threats online"},
    {question: "Someone posts mean things about you or calls you names"},
    {question: "Your friend has written nasty things about someone else"},
    {question: "Your friend posts rumors about you or private, embarrassing, or sexual things about you"},
    {question: "Someone you know keeps sending you texts or posts on your wall or feed when you do not want them to"},
    {question: "Someone you don’t know sends you mean messages or posts mean things about you"}
  ]
)

# Parenting - Praise
deck = Deck.find(15)
deck.cards.create(
  [
    {vimeo_url: "102248155", question:"Young Child"},
    {vimeo_url: "102248157", question:"Siblings"},
  ]
)

# Parenting - Contingency
deck = Deck.find(16)
deck.cards.create(
  [
    {vimeo_url: "102247673", question:"Younger Child Part 1"},
    {vimeo_url: "102247869", question:"Younger Child Part 2"},
    {vimeo_url: "102247870", question:"Younger Child Part 3"},
    {vimeo_url: "102248150", question:"Teenager Part 1"},
    {vimeo_url: "102248152", question:"Teenager Part 2"}
  ]
)

# Parenting - Removal of Privileges
deck = Deck.find(17)
deck.cards.create(
  [
    {vimeo_url: "102247669", question:"Younger Child (Girl)"},
    {vimeo_url: "102247671", question:"Teenager"},
    {vimeo_url: "102247672", question:"Younger Child (Boy)"}
  ]
)

# Parenting - Active Ignoring
deck = Deck.find(18)
deck.cards.create(
  [
    {vimeo_url: "102247668", question:"Active Ignoring"}
  ]
)

# Parenting - ABC Model
deck = Deck.find(19)
deck.cards.create(
  [
    {vimeo_url: "89637911", question:"Expert Introduction"},
    {vimeo_url: "92511739", question:"Teenager Arguing with Parent"},
    {vimeo_url: "92330682", question:"Younger Child Talking Back to Parent"},
    {vimeo_url: "92352114", question:"School Refusal"},
    {vimeo_url: "92157071", question:"Older Sibling Fighting with Younger Sibling"},
    {vimeo_url: "92516467", question:"Teenager Breaking Curfew"}
  ]
)


c4s2 = Section.where(chapter_id: 4, sequence: 2).first
c4s2.section_questions.create(
  sequence: 1,
  question: "have witnessed serious violence in their neighborhood or school",
  answer_boys: 40,
  answer_girls: 36
)
c4s2.section_questions.create(
  sequence: 2,
  question: "have been physically abused by a caregiver",
  answer_boys: 12,
  answer_girls: 13
)
c4s2.section_questions.create(
  sequence: 3,
  question: "have been physically assaulted by someone other than a caregiver",
  answer_boys: 19,
  answer_girls: 12
)
c4s2.section_questions.create(
  sequence: 4,
  question: "have been sexually abused/assaulted",
  answer_boys: 10,
  answer_girls: 12
)
c4s2.section_questions.create(
  sequence: 5,
  question: "have seen or heard one of their caregivers hurt the other",
  answer_boys: 8,
  answer_girls: 10
)
c4s2.section_questions.create(
  sequence: 6,
  question: "have had a close friend or family member die unexpectedly due to violence or accident",
  answer_boys: 16,
  answer_girls: 22
)
c4s2.section_questions.create(
  sequence: 7,
  question: "have been in a serious accident such as a car crash or injury",
  answer_boys: 11,
  answer_girls: 9
)
c4s2.section_questions.create(
  sequence: 8,
  question: "have been affected by a disaster",
  answer_boys: 28,
  answer_girls: 29
)
c4s2.section_questions.create(
  sequence: 9,
  question: "have had at least one of these bad things happen",
  answer_boys: 67,
  answer_girls: 65
)
c4s2.section_questions.create(
  sequence: 10,
  question: "have had more than one of these bad things happen",
  answer_boys: 37,
  answer_girls: 39
)

c4s3 = Section.where(chapter_id: 4, sequence: 3).first
c4s3.section_questions.create(
  sequence: 1,
  question: "have had problems with depression or sadness",
  answer_boys: 6,
  answer_girls: 14
)
c4s3.section_questions.create(
  sequence: 2,
  question: "have had problems with anxiety",
  answer_boys: 8,
  answer_girls: 20
)
c4s3.section_questions.create(
  sequence: 3,
  question: "have had problems with nightmares, avoidance, or stress after a major event",
  answer_boys: 7,
  answer_girls: 14
)
c4s3.section_questions.create(
  sequence: 4,
  question: "have had problems with behavior at home or in school",
  answer_boys: 27,
  answer_girls: 14
)
c4s3.section_questions.create(
  sequence: 5,
  question: "have had problems with alcohol",
  answer_boys: 7,
  answer_girls: 7
)

#add some notes

# domestic violence deck
deckDV = Deck.find(2)
deckDV.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>Cannot tell just by looking at the child or caregiver(s)</li><li>Sometimes there will be a change in the child/adolescent's behavior or mood</li></ul>"}
	]
)
deckDV.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>A parent/caregiver or other trusted adult (give examples: neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
	]
)
deckDV.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>An adult they trust (e.g., therapist, teacher, neighbor, relative, clergy, coach)</li><li>Can call 911 or go to a safe place (like a trusted neighbor)</li></ul>"}
	]
)
deckDV.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>No right or wrong way to feel. Child can have many different feelings (e.g., sad, scared, mad, ashamed, embarrassed)</li><li>Can also have different and mixed feelings about the person being hurt and the person who is being violent</li><li>No matter how a child feels about domestic violence – it’s okay to have these feelings and it’s important to share them</li></ul>"}
	]
)
deckDV.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>Domestic violence is the fault of the person who is violent.</li><li>The person who starts the violence is responsible, not the victims who may be violent to defend themselves</li><li>It’s okay for grown-ups to get angry at each other or at children, but NOT OK okay  for grown-ups to be violent toward others</li></ul>"}
	]
)
deckDV.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>Tell someone else – another adult, neighbor, friend, teacher, coach, clergy</li><li>Call 911</li><li>The child should NOT try to stop the violence by getting in the middle of the fight</li><li>Go to a safe place (neighbor’s house, etc.)</li></ul>"}
	]
)
deckDV.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>Do NOT try to stop the violence by getting in the middle of the fight</li><li>Go to a safe place (neighbor or friend’s house)</li><li>Call 911</li></ul>"}
	]
)
deckDV.cards.offset(7)[0].notes.create(
	[
		{content: "<ul><li>When an adult family member hurts or is violent toward another family member</li><li>Examples of physical violence are: Hitting, kicking, punching, biting, choking, shoving</li><li>Examples of verbal/psychological violence are: Name calling, threatening to hurt or kill the person or themselves</li><li>Examples of property violence are: Destroying, throwing, breaking someone’s things or harming someone’s pet</li><li>Examples of sexual violence are: Touching the person’s private parts or making the person touch their private parts when the person says no or is showing they don’t want to be touched in that way</li></ul>"}
	]
)
deckDV.cards.offset(8)[0].notes.create(
	[
		{content: "<ul><li>All grown-ups get into arguments, and arguments are okay, but it’s not ok to physically harm someone else, or fight by destroying someone’s property (i.e., want to convey to child that non-physical fighting is normal, but not fighting that causes physical or significant emotional harm)</li></ul>"}
	]
)
deckDV.cards.offset(9)[0].notes.create(
	[
		{content: "<ul><li>Note: these are things a child “might” do, as opposed to a list of recommendations around what they “should” do</li><li>May try to stop the fight by getting in the middle of it</li><li>May yell at the adults to stop; or try to distract the adults to prevent the fight</li><li>May run away and hide in another room</li><li>May leave the house</li><li>May try to pretend it’s not happening</li></ul>"}
	]
)
deckDV.cards.offset(10)[0].notes.create(
	[
		{content: "<ul><li>Some don’t tell because they’re too ashamed, embarrassed, sad or scared</li><li>Some are told  by the person who is violent or by the person who was hurt not to tell about the violence</li><li>May have seen so much violence they do not realize it is wrong and don’t know they should tell or can get help</li></ul>"}
	]
)
deckDV.cards.offset(11)[0].notes.create(
  [
    {content: "<ul><li>People have problems dealing with anger</li><li>They deal with anger by being violent</li><li>They don’t know any other way to deal with problems in their relationships</li><li>They want to have control over someone else</li></ul>"}
  ]
)


#sexual abuse
deckSA = Deck.find(3)
deckSA.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>Penis, testicles, bottom, buttocks</li></ul>"}
	]
)
deckSA.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>Breasts, vagina, bottom, buttocks</li></ul>"}
	]
)
deckSA.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>It's not your fault</li><li>It's the responsibility of adults</li><li>Don't be afraid to talk to someone, like a friend, teacher, neighbor, relative, therapist</li><li>You are not alone</li></ul>"}
	]
)
deckSA.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>At the doctor’s office</li><li>If medication is needed</li><li>For a young child—if help is needed during bath time or getting changed/dressed</li></ul>"}
	]
)
deckSA.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>May feel many different feelings (sad, confused, alone, special, etc.)</li><li>For some the touching may feel good</li><li>The child may still have positive feelings towards the person who abused them. Some may feel angry at the person, or scared, or guilty about what happened</li><li>Any of these feelings are okay</li></ul>"}
	]
)
deckSA.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>May not want to sleep or be alone</li><li>Sometimes get into more arguments</li><li>May feel sad and want to be alone</li><li>May not want to tell someone what happened because embarrassed or ashamed or afraid</li></ul>"}
	]
)
deckSA.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>You can't tell just by looking</li><li>Sometimes you can tell by the way they are acting that something is bothering them, but you don't know what</li></ul>"}
	]
)
deckSA.cards.offset(7)[0].notes.create(
	[
		{content: "<ul><li>No child is responsible for what an adult (or older child) does.</li></ul>"}
	]
)
deckSA.cards.offset(8)[0].notes.create(
	[
		{content: "<ul><li>Another adult like a parent, other family member, or teacher, therapist</li><li>Important to keep telling until someone listens and helps</li></ul>"}
	]
)
deckSA.cards.offset(9)[0].notes.create(
	[
		{content: "<ul><li>There are a lot of different reasons</li><li>No single explanation for why happens to any child</li><li>No child is responsible for what an adult does</li></ul>"}
	]
)
deckSA.cards.offset(10)[0].notes.create(
	[
		{content: "<ul><li>They may worry about getting pregnant, getting a sexually transmitted disease, being 'damaged'</li><li>They may worry that others will know just by looking at them.</li><li>They may think they're different and that they caused the abuse to happen</li><li>They may worry about losing their virginity</li><li>They may think no-one will ever care about or love them</li></ul>"}
	]
)
deckSA.cards.offset(11)[0].notes.create(
	[
		{content: "<ul><li>The offender may tell the child to keep the abuse a secret</li><li>Offender may say it’s the child’s fault or that the child or their family will get hurt if they tell</li><li>Child may feel ashamed, embarrassed, or scared</li><li>Child may think no-one will believe them</li><li>Child may fear being removed from their home</li><li>Child may worry that the offender will go to jail</li></ul>"}
	]
)
deckSA.cards.offset(12)[0].notes.create(
	[
		{content: "<ul><li>When an adult or older child touches, or rubs a child's private parts</li><li>When an adult or older child asks the child to touch or rub their private parts</li><li>Offender may be someone you know, a relative, a stranger, or another child</li><li>Can include behaviors that do not involve the offender touching the child</li></ul><h3>Examples</h3><ul><li>Talking about private parts</li><li>Showing private parts or pictures of private parts</li><li>Making a child do things with his or her own private parts</li><li>Making a child watch others engage in sexual acts</li></ul>"}
	]
)
deckSA.cards.offset(13)[0].notes.create(
	[
		{content: "<ul><li>Yes - in private</li></ul>"}
	]
)
deckSA.cards.offset(14)[0].notes.create(
	[
		{content: "<ul><li>Sexual abuse is when an adult touches a child's private parts</li><li>Sexual abuse is when someone touches you or does something to your private sexual parts when you didn't want them to</li><li>Sex is between two people who both agree and want to engage in the activity. This is between adults of similar age. There is no force involved.</li></ul>"}
	]
)

#physical abuse
deckPA = Deck.find(4)
deckPA.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>They may be aggressive/hurtful towards others</li><li>They may cry</li><li>They may become withdrawn/quiet</li><li>They may act like nothing bothers them</li></ul>"}
	]
)
deckPA.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>Might feel stressed out, angry, frustrated. Some parents may not have a lot of negative feelings because they believe that this is the right thing to do</li></ul>"}
	]
)
deckPA.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>Do not know any other way to discipline their children</li><li>Parents may be under a lot of stress and they take it out on others</li><li>Some were abused by their parents and learned that this is an okay way to discipline their own children</li></ul>"}
	]
)
deckPA.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>Physical abuse is NEVER the child’s fault</li><li>Even if a child does something wrong or misbehaves, it is not okay for them to be physically abused</li><li>Child’s behavior does not cause the abuse</li></ul>"}
	]
)
deckPA.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>Focus on behavior and physical characteristics of emotions</li><li>No clear right or wrong answers</li></ul>"}
	]
)
deckPA.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>Physical abuse occurs in many families across the US</li><li>Can happen in any family</li><li>Happens to boys and girls</li><li>Does not matter what color a family is, how much money they make, where they live, or their religious beliefs</li></ul>"}
	]
)
deckPA.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>A trusted adult (teacher, neighbor, friend, other family member, coach, clergy)</li></ul>"}
	]
)
deckPA.cards.offset(7)[0].notes.create(
	[
		{content: "<ul><li>When a parent/caregiver, or another adult who is caring for the child hurts or causes physical injury, like red marks, bruises, cuts, or broken bones</li></ul>"}
	]
)
deckPA.cards.offset(8)[0].notes.create(
	[
		{content: "<ul><li>May feel sad, scared, lonely, mad</li><li>May feel embarrassed because they think they did something wrong and that the abuse is their fault</li><li>Fearful of the person who hurt them</li><li>May love and be angry at the person who hurt them at the same time</li><li>No matter how a child feels, all feelings are okay! (there is no right or wrong way to feel)</li></ul>"}
	]
)
deckPA.cards.offset(9)[0].notes.create(
	[
		{content: "<ul><li>Time-out</li><li>Taking away things that they like</li><li>Requiring the child to do extra chores</li></ul>"}
	]
)
deckPA.cards.offset(10)[0].notes.create(
	[
		{content: "<ul><li>Sad, worthless, dumb (depending on what names they’re being called), lonely, mad</li><li>Not loved</li><li>Ashamed, embarrassed</li></ul>"}
	]
)
deckPA.cards.offset(11)[0].notes.create(
	[
		{content: "<ul><li>Scared that their parents will be mad at them</li><li>They don’t want to get their parents in trouble</li><li>Others may think it is okay for a grown-up to physically hurt them in some way</li><li>Child may think nobody will believe them</li><li>Child may fear being removed from their home</li><li>Child may worry that the offender will go to jail</li></ul>"}
	]
)
deckPA.cards.offset(12)[0].notes.create(
	[
		{content: "<ul><li>Spanking is a form of physical punishment/discipline used as a way to stop or change a child’s behavior</li><li>Physical punishment is not against the law but other forms of discipline are much more effective</li><li>Physical abuse causes red marks, bruises, cuts, broken bones, or other injuries.</li></ul>"}
	]
)

#personal safety
deckPS = Deck.find(5)
deckPS.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>Touches that hurt</li><li>Touches when you don't want them</li><li>Touches on your private sexual parts</li></ul>"}
	]
)
deckPS.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>Hugs</li><li>High fives</li><li>Fist bumps</li><li>Pat on the back</li></ul>"}
	]
)
deckPS.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>Some children feel embarrassed, ashamed, mad, sad, confused</li></ul>"}
	]
)
deckPS.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>It's not ok for someone to tell you to keep touches a secret</li></ul>"}
	]
)
deckPS.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>Practice this activity with the child. Demonstrate if necessary.</li></ul>"}
	]
)
deckPS.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>Answers are unique to the child's history and circumstances</li></ul>"}
	]
)
deckPS.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>Tell someone else.</li><li>Keep telling until someone listens. </li><li>Talk to your teacher, a clergy, a neighbor, a friend.</li></ul>"}
	]
)
deckPS.cards.offset(7)[0].notes.create(
	[
		{content: "<ul><li>No right or wrong answers.</li></ul>"}
	]
)
deckPS.cards.offset(8)[0].notes.create(
	[
		{content: "<ul><li>Tell someone you trust (give examples - neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
	]
)
deckPS.cards.offset(9)[0].notes.create(
	[
		{content: "<ul><li>Tell someone you trust (give examples - neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
	]
)
deckPS.cards.offset(10)[0].notes.create(
	[
		{content: "<ul><li>Tell someone you trust (give examples - neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
	]
)
deckPS.cards.offset(11)[0].notes.create(
	[
		{content: "<ul><li>Don't forget to unplug the phone first</li></ul>"}
	]
)
deckPS.cards.offset(12)[0].notes.create(
	[
		{content: "<ul><li>Good, proud, embarrassed, confused, disbelieving, etc</li></ul>"}
	]
)

#Disasters and Traumatic Grief
deckDTG = Deck.find(6)
deckDTG.cards.offset(0)[0].notes.create(
  [
    {content: "<ul><li>A parent/caregiver or other trusted adult (give examples: neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
  ]
)
deckDTG.cards.offset(1)[0].notes.create(
  [
    {content: "<ul><li>No right or wrong way to feel. Child can have  many different feelings (e.g., sad, scared, mad, confused, desperate, unlucky)</li></ul>"}
  ]
)
deckDTG.cards.offset(2)[0].notes.create(
  [
    {content: "<ul><li>All grown-ups get into arguments, but it’s not ok to physically harm someone else, or fight by destroying someone’s property (i.e., want to convey to child that fighting is normal, but not fighting that causes physical or significant emotional harm)</li></ul>"}
  ]
)
deckDTG.cards.offset(3)[0].notes.create(
  [
    {content: "<ul><li>Some kids may say that bad things always happen to them and their family</li><li>Some kids may say that it’s God’s will or that they deserved it.</li><li>Want to convey that sometimes things happen for no good reason; no clear/good explanation)</li><li>Note: specific for natural disasters to try to capture children’s understanding/explanations for why natural disasters occur</li></ul>"}
  ]
)
deckDTG.cards.offset(4)[0].notes.create(
  [
    {content: "<ul><li>May run away and hide in another room</li><li>May leave the house; not want to be at home</li><li>May try to pretend it didn’t happen/nothing has changed</li><li>May not want to sleep or be alone</li><li>Sometimes get into more arguments</li><li>May feel sad and want to be alone</li><li>May not want to talk about it</li></ul>"}
  ]
)
deckDTG.cards.offset(5)[0].notes.create(
  [
    {content: "<ul><li>May leave the house; not want to be at home</li><li>May try to pretend it didn’t happen/nothing has changed</li><li>Sometimes get into more arguments</li><li>May feel sad and want to be alone</li><li>May not want to talk about it</li></ul>"}
  ]
)
deckDTG.cards.offset(6)[0].notes.create(
  [
    {content: "<ul><li>Scared that someone else close (e.g., other parent, another sibling, self) might die unexpectedly</li><li>Worried about who will take care of them if the other parent dies (if a parent died)</li><li>Afraid that the person suffered</li><li>Worried that they did something to cause the death or didn't do enough to prevent it</li></ul>"}
  ]
)
deckDTG.cards.offset(7)[0].notes.create(
  [
    {content: "<ul><li>Not tell the child the truth about how the person died</li><li>Not talk about the person who died, or talk about how they died</li><li>Act like everything is okay or normal</li><li>Not talk about feelings, or act like the child shouldn't talk about their feelings</li></ul>"}
  ]
)
deckDTG.cards.offset(8)[0].notes.create(
  [
    {content: "<ul><li>Help them understand how the person died</li><li>Talk about the person who died when the child is ready</li><li>Talk about their feelings and let the child know that he can talk about his feelings too</li><li>Let the child know that they are there to take care of him and keep him safe</li></ul>"}
  ]
)

#serious accidents
deckSA = Deck.find(7)
deckSA.cards.offset(0)[0].notes.create(
  [
    {content: "<ul><li>A parent/caregiver or other trusted adult (give examples: neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
  ]
)
deckSA.cards.offset(1)[0].notes.create(
  [
    {content: "<ul><li>No right or wrong way to feel. Children can have many different feelings (e.g., sad, scared, mad, confused, desperate, unlucky)</li></ul>"}
  ]
)
deckSA.cards.offset(2)[0].notes.create(
  [
    {content: "<ul><li>All grown-ups get into arguments, but it’s not ok to physically harm someone else, or fight by destroying someone’s property (i.e., want to convey to child that fighting is normal, but not fighting that causes physical or significant emotional harm)</li></ul>"}
  ]
)
deckSA.cards.offset(3)[0].notes.create(
  [
    {content: "<ul><li>Parent/caregiver or other trusted adult (give examples: neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
  ]
)
deckSA.cards.offset(4)[0].notes.create(
  [
    {content: "<ul><li>Some kids may say that bad things always happen to them/their family; that it’s god’s will; that they deserved it. Want to convey that sometimes things happen for no good reason; no clear/good explanation</li></ul>"}
  ]
)
deckSA.cards.offset(5)[0].notes.create(
  [
    {content: "<ul><li>May run away and hide in another room</li><li>May leave the house; not want to be at home</li><li>May try to pretend it didn’t happen/nothing has changed</li><li>May not want to sleep or be alone</li><li>Sometimes get into more arguments</li><li>May feel sad and want to be alone</li><li>May not want to talk about it</li></ul>"}
  ]
)
deckSA.cards.offset(6)[0].notes.create(
  [
    {content: "<ul><li>May leave the house; not want to be at home</li><li>May try to pretend it didn’t happen/nothing has changed</li><li>Sometimes get into more arguments</li><li>May feel sad and want to be alone</li><li>May not want to talk about it</li></ul>"}
  ]
)


#bullying/peer victimization
deckBPV = Deck.find(8)
deckBPV.cards.offset(0)[0].notes.create(
  [
    {content: "<ul><li>They may be aggressive/hurtful towards others</li><li>They may cry</li><li>They may become withdrawn/quiet</li><li>They may act like nothing bothers them</li></ul>"}
  ]
)
deckBPV.cards.offset(1)[0].notes.create(
  [
    {content: "<ul><li>Parent/caregiver or other trusted adult (give examples: neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
  ]
)
deckBPV.cards.offset(2)[0].notes.create(
  [
    {content: "<ul><li>No right or wrong way to feel. Child can have many different feelings (e.g., sad, scared, mad, confused, desperate, unlucky, dumb, alone)</li></ul>"}
  ]
)
deckBPV.cards.offset(3)[0].notes.create(
  [
    {content: "<ul><li>It is NEVER the victim’s fault. If a child is being bullied they need to tell a parent/caregiver or other trusted adult (give examples: neighbor, teacher, friend, another adult, clergy, coach)</li></ul>"}
  ]
)
deckBPV.cards.offset(4)[0].notes.create(
  [
    {content: "<ul><li>Bullying is widespread in the United States</li><li>In a 2011 nationwide survey, 20% of high school students (1 out of every 5) reported being bullied at school</li><li>An estimated 16% of high school students (1 out of every 6) reported in 2011 that they were bullied electronically (e.g., internet, social site, email, texting)</li></ul>"}
  ]
)
deckBPV.cards.offset(5)[0].notes.create(
  [
    {content: "<ul><li>Bully is being abused him/herself</li><li>Peer pressure to bully</li><li>Copying the behavior they see or experience at home</li><li>Become frustrated easily and don’t manage anger well</li></ul>"}
  ]
)
deckBPV.cards.offset(6)[0].notes.create(
  [
    {content: "<ul><li>That it will never stop</li><li>That no-one cares about them</li><li>That there’s nothing they can do</li></ul>"}
  ]
)
deckBPV.cards.offset(7)[0].notes.create(
  [
    {content: "<ul><li>Angry, scared, inferior</li></ul>"}
  ]
)
deckBPV.cards.offset(8)[0].notes.create(
  [
    {content: "<ul><li>The bully may have threatened them not to tell – that he or she would get hurt if they tell</li><li>Scared to tell</li><li>Embarrassed</li><li>Don’t think anyone will believe them</li><li>That it’s no big deal; happens to lots of kids</li><li>That there’s nothing anyone can do</li></ul>"}
  ]
)
deckBPV.cards.offset(9)[0].notes.create(
  [
    {content: "<ul><li>Being pushed, shoved, harassed by other kids</li><li>Being made fun of</li><li>Being called names</li><li>It can occur in person, on the phone, in texts, on-line, on facebook (twitter, other social media)</li></ul>"}
  ]
)


#substance abuse
deckOK = Deck.find(9)
deckOK.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>Holding hands to cross the street</li><li>Asking for a hug when you want it</li><li>High five</li><li>Handshake</li></ul>"}
	]
)
deckOK.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>Holding hands to cross the street</li><li>Asking for a hug when you want it</li><li>High five</li><li>Handshake</li></ul>"}
	]
)
deckOK.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>Encourage child to tell a trusted adult (e.g., coach, parent, teacher, priest, 911). Help child identify which people are good to go to in various situations.</li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays. Caregivers could be included in role-plays.</li><li>Practice saying \"NO\" and getting away from the person. Review \"NO-GO-TELL\" with younger children.</li><li>Review and role-play how to respond assertively (making eye-contact; using respectful and firm language -- \"Please Stop!\" \"Don't touch me!\" -- making voice loud enough to be understood; using body language that is calm, aware, and confident)</li><li>It is always a good idea to develop safety plan with help of caregiver</li><li>May need to involve other family members who may take on different safety roles.</li></ul>"}
	]
)
deckOK.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>Discuss how it is sometimes ok for a doctor to ask a patient to take off their clothes or to examine private parts if the examination requires it.</li><li>Remind the child to talk to his/her caregiver if he/she feels uncomfortable.</li><li>Discuss how sometimes it is hard to tell if a touch is ok or not ok.</li><li>Encourage child to talk to a grown-up about \"confusing touches\" to decide if it is ok or not. Practice role-playing with caregiver.</li></ul>"}
	]
)
deckOK.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>Review: 'okay touches' feel good and therefore it is sometimes ok for a teacher to touch a child on the shoulder. However, remind the child that it is not okay if he/she feels uncomfortable.</li><li>Discuss how sometimes it is hard to tell if a touch is ok or not ok.</li><li>Encourage child to talk to a grown-up about 'confusing touches' to decide if it is ok or not. Practice role-playing with caregiver.</li></ul>"}
	]
)
deckOK.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>Review: 'okay touches' feel good and therefore it is sometimes ok for a teacher to touch a child on the shoulder. However, remind the child that it is not okay if he/she feels uncomfortable.</li><li>Discuss how sometimes it is hard to tell if a touch is ok or not ok.</li><li>Encourage child to talk to a grown-up about 'confusing touches' to decide if it is ok or not. Practice role-playing with caregiver.</li></ul>"}
	]
)
deckOK.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>Encourage child to tell a trusted adult (e.g., coach, parent, teacher, priest, 911). Help child identify which people are good to go to in various situations.</li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays.Caregivers could be included in role-plays. Practice saying \"NO\" and getting away from the person. Review \"NO-GO-TELL\" with younger children.</li><li>Review and role-play how to respond assertively (making eye-contact; using respectful and firm language -- \"Please Stop!\" \"Don't touch me!\" -- making voice loud enough to be understood; using body language that is calm, aware, and confident)</li><li>It is always a good idea to develop safety plan with help of caregiver</li><li>May need to involve other family members who may take on different safety roles.</li></ul>"}
	]
)
deckOK.cards.offset(7)[0].notes.create(
	[
		{content: "<ul><li>Encourage child to tell a trusted adult (e.g., coach, parent, teacher, priest, 911). Help child identify which people are good to go to in various situations.</li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays.Caregivers could be included in role-plays. Practice saying \"NO\" and getting away from the person. Review \"NO-GO-TELL\" with younger children.</li><li>Practice saying \"NO\" and getting away from the person. Review and role-play \"NO-GO-TELL\" with younger children.</li><li>Review and role-play how to respond assertively (making eye-contact; using respectful and firm language -- \"Please Stop!\" \"Don't touch me!\" -- making voice loud enough to be understood; using body language that is calm, aware, and confident)</li><li>It is always a good idea to develop safety plan with help of caregiver</li><li>May need to involve other family members who may take on different safety roles.</li></ul>"}
	]
)
deckOK.cards.offset(8)[0].notes.create(
	[
		{content: "<ul><li>Encourage child to tell a trusted adult (e.g., coach, parent, teacher, priest, 911). Help child identify which people are good to go to in various situations.</li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays. Caregivers could be included in role-plays.</li><li>Review how to assess for danger cues.</li><li>Discuss with child the dangers of trying to fight back. Emphasize to the child \"needing to do what you must to survive.\"</li><li>Review safe places to go to as soon as the child can (e.g., home, neighbor's house).</li><li>It is always a good idea to develop safety plan with help of caregiver</li><li>May need to involve other family members who may take on different safety roles.</li></ul>"}
	]
)
deckOK.cards.offset(9)[0].notes.create(
	[
		{content: "<ul><li>Review that \"okay touches\" feel good and that sometimes it feels ok to wrestle with friends. Remind the child that it is not okay if s/he feels uncomfortable.</li><li>Encourage child to talk to a grown-up about \"confusing touches\" to decide if it is ok or not. Practice role-playing with caregiver.</li><li>Role-play how to respond assertively (making eye-contact; using respectful and firm language -- \"Please Stop!\" \"Don't touch me!\" -- making voice loud enough to be understood; using body language that is calm, aware, and confident)</li><li>It is always a good idea to develop safety plan with help of caregiver.</li></ul>"}
	]
)
deckOK.cards.offset(10)[0].notes.create(
	[
		{content: "<ul><li>Review safety strategies such as checking with parent before accepting ride.</li><li>It is always a good idea to develop safety plan with help of caregiver (e.g., come up with password)</li></ul>"}
	]
)

#substance abuse
deckSA = Deck.find(10)
deckSA.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>Discuss and role-play refusal skills. Remind child that each child is different--what works for one may not work for another. The child should choose what he/she is comfortable with:</li><li>Say \"no thanks\"</li><li>Give a reason or excuse</li><li>Walk away</li><li>Change the subject</li><li>Use humor</li><li>Surround yourself with friends who would decline the drink</li><li>Identify individuals that child can lean on for help (e.g., caregiver, friend's caregiver)</li><li>It is always a good idea to develop safety plan with help of caregiver</li></ul>"}
	]
)
deckSA.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>Discuss and role-play refusal skills. Remind child that each child is different--what works for one may not work for another. The child should choose what he/she is comfortable with:</li><li>Say \"no thanks\"</li><li>Give a reason or excuse</li><li>Walk away</li><li>Change the subject</li><li>Use humor</li><li>Surround yourself with friends who would decline the marijuana</li><li>Identify individuals that child can lean on for help (e.g., caregiver, friend's caregiver)</li></ul>"}
	]
)
deckSA.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>Identify individuals that child can lean on for help (e.g., caregiver, friend's caregiver)</li><li>Discuss and role-play possible steps to keep friend from driving including:</li><li>Taking/hiding car keys</li><li>Calling a cab or another sober person for a ride</li><li>Telling your friend that you care about him/her and that you don't want to see him/her get hurt</li><li>Ask other friends to help you convince your friend not to drive</li><li>Call 911 if your friend persists to get in the car and drive</li></ul>"}
	]
)
deckSA.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>Identify individuals that child can lean on for help (e.g., caregiver, friend's caregiver</li><li>Discuss and role-play refusal skills. Remind child that each child is different--what works for one may not work for another. The child should choose what he/she is comfortable with:</li><li>Say \"no thanks\"</li><li>Give a reason or excuse</li><li>Walk away</li><li>Change the subject</li><li>Use humor</li></ul>"}
	]
)
deckSA.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>Identify individuals that child can lean on for help (e.g., caregiver, friend's caregiver</li><li>Discuss and role-play refusal skills. Remind child that each child is different--what works for one may not work for another. The child should choose what he/she is comfortable with:</li><li>Say \"no thanks\"</li><li>Give a reason or excuse</li><li>Walk away</li><li>Change the subject</li><li>Use humor</li></ul>"}
	]
)
deckSA.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>Discuss risks of accepting drinks from others, especially strangers.</li><li>Drug and alcohol-facilitated sexual assault is prevalent.</li><li>Discuss and role-play refusal skills. Remind child that each child is different--what works for one may not work for another. The child should choose what he/she is comfortable with:</li><li>Say \"no thanks\"</li><li>Give a reason or excuse</li><li>Walk away</li><li>Change the subject</li><li>Use humor</li></ul>"}
	]
)
deckSA.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>Identify individuals that child can lean on for help (e.g., caregiver, friend's caregiver</li><li>Discuss and role-play refusal skills. Remind child that each child is different--what works for one may not work for another. The child should choose what he/she is comfortable with:</li><li>Say \"no thanks\"</li><li>Give a reason or excuse</li><li>Walk away</li><li>Change the subject</li><li>Use humor</li></ul>"}
	]
)

#peer victimization
deckPV = Deck.find(11)
deckPV.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>Encourage the child to tell a trusted adult (e.g., coach, parent, teacher). </li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays.</li><li>Role-play how to respond assertively</li></ul>"}
	]
)
deckPV.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>Encourage the child to tell a trusted adult (e.g., coach, parent, teacher).</li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays Caregivers can be included in role-plays</li><li>Role-play how to respond assertively</li></ul>"}
	]
)
deckPV.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>Encourage the child to tell a trusted adult (e.g., coach, parent, teacher). </li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays. Caregivers can be included in role-plays</li><li>Assess threats of interjecting</li><li>Role-play how to respond assertively as appropriate</li></ul>"}
	]
)
deckPV.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>Encourage the child to tell a trusted adult (e.g., coach, parent, teacher). </li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays. Caregivers can be included in role-plays </li><li>Discuss child's interpretation; reframe as appropriate</li></ul>"}
	]
)
deckPV.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>Discuss with child how this can be another form of bullying</li><li>Role-play possible responses could include: changing the subject; telling them you do not feel comfortable talking about someone that way; walk away from conversation</li></ul>"}
	]
)
deckPV.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>Discuss the dangers of accepting rides from strangers</li><li>Practice how to assertively say \"no\"</li><li>If stranger persists, discuss other course of action</li><li>Running away from the person</li><li>Yelling \"help this person is trying to take me\"</li><li>Going to another adult until stranger leaves</li><li>Telling caregiver or other trusted adult of what happened</li><li>It is always a good idea to develop a safety plan with the help of caregiver</li></ul>"}
	]
)
deckPV.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>Role-play how to assertively let the other person know that child is not interested</li></ul>"}
	]
)

#witnessed violence
deckWV = Deck.find(12)
deckWV.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses. For example:</li><li>Going to safe place as soon as the child can (e.g., home, neighbor's house).</li><li>Telling a caregiver or other trusted adult</li><li>Calling 911</li></ul>"}
	]
)
deckWV.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>Encourage child to tell a trusted adult (e.g., coach, parent, teacher, priest, 911). Help child identify which people are good to go to in various situations.</li><li>Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays.</li><li>Discuss going to safe place as soon as the child can (e.g., neighbor's house)</li><li>Discuss with child the dangers of trying to intervene</li></ul>"}
	]
)
deckWV.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses. For example:</li><li>Staying in bedroom</li><li>Using relaxation techniques</li><li>Discuss possible responses if behavior escalates. For example:</li><li>Call 911 if there is imminent danger to you or a parent</li><li>Identifying a safe place the child can go (e.g., neighbor's house)</li><li>Discuss with child the dangers of trying to intervene</li></ul>"}
	]
)
deckWV.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses. For example:</li><li>Going to safe place as soon as the child can (e.g., home, neighbor's house)</li><li>Call 911</li><li>Discuss with child the dangers of trying to intervene</li></ul>"}
	]
)
deckWV.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses. For example:</li><li>Talking with parent while sober about how the drinking makes the child feel</li><li>Avoid arguing with a drunk parent.</li><li>Going to safe place (e.g., bedroom)</li><li>Discuss what to do if parent becomes abusive:</li><li>Role-play how to respond assertively if feeling uncomfortable (make eye-contact; use respectful and firm language; use body language that is calm, aware, and confident)</li><li>Call 911</li><li>Going to safe place (e.g., neighbor's house)</li><li>Telling another trusted adult (e.g., teacher, coach, priest). Emphasize TELLING until someone listens and HELPS. Practice this skill using role-plays. Caregivers can be included in role-plays.</li></ul>"}
	]
)
deckWV.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses. For example:</li><li>Going to safe place as soon as the child can (e.g., home, neighbor's house).</li><li>Telling a caregiver or other trusted adult. Calling 911</li></ul>"}
	]
)
deckWV.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses. For example:</li><li>Telling a teacher or other trusted adult (e.g., coach, principle, caregiver.)</li><li>Calling 911</li></ul>"}
	]
)
deckWV.cards.offset(7)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses. For example:</li><li>Safety behaviors (e.g., getting down, taking cover, avoiding windows)</li><li>Going to safe place as soon as the child can (e.g., home, neighbor's house).</li><li>Telling a caregiver or other trusted adult</li><li>Calling 911</li></ul>"}
	]
)

#internet safety
deckIS = Deck.find(13)
deckIS.cards.offset(0)[0].notes.create(
	[
		{content: "<ul><li>Discuss dangers of giving out your personal information (e.g., potentially dangerous person will know where you live)</li><li>Discuss importance of never giving out personal information (e.g., home address, school name, telephone number, email or social media account password)</li></ul>"}
	]
)
deckIS.cards.offset(1)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses:</li><li>Let the person know you do not feel comfortable and ask them respectfully to stop</li><li>Block person from account if they persist</li><li>Talk to a trusted adult</li></ul>"}
	]
)
deckIS.cards.offset(2)[0].notes.create(
	[
		{content: "<ul><li>Discuss importance of never giving out personal information (e.g., home address, school name, telephone number, email or social media account password)</li><li>Discuss possible refusal responses:</li><li>Say \"sorry I can't\"</li><li>Give a reason or excuse (e.g., \"my parents are really strict about that and I don't want to get my computer/phone taken away.\")</li><li>Change the subject</li><li>Use humor</li></ul>"}
	]
)
deckIS.cards.offset(3)[0].notes.create(
	[
		{content: "<ul><li>Discuss potential dangers of meeting someone online. Review steps to minimize risks:</li><li>Telling trusted adult of plans to meet person</li><li>Have a trusted adult help you look up information about the person to make sure they are who they say they are</li><li>Arrange to meet somewhere public</li><li>Bring two or more friends or a trusted adult</li></ul>"}
	]
)
deckIS.cards.offset(4)[0].notes.create(
	[
		{content: "<ul><li>Discuss possible responses:</li><li>Let the person know you feel uncomfortable and ask them respectfully to stop</li><li>Block person from account if they persist</li><li>Talk to a trusted adult</li><li>Do not forward the pictures to other people</li></ul>"}
	]
)
deckIS.cards.offset(5)[0].notes.create(
	[
		{content: "<ul><li>Discuss how this is ok unless you feel uncomfortable. </li><li>If child feels uncomfortable, discuss potential responses:</li><li>Letting the person know you do not feel comfortable and asking them to please stop</li><li>Deleting pictures from account and not circulating picture to other people</li><li>Blocking person from account if they persist</li><li>Talking to a trusted adult </li></ul>"}
	]
)
deckIS.cards.offset(6)[0].notes.create(
	[
		{content: "<ul><li>Discuss how this is always a bad idea and highlight potential problems:</li><li>Friend will circulate pictures to others</li><li>Pictures may be posted on Facebook or other social media</li><li>Parents may find out</li></ul>"}
	]
)
deckIS.cards.offset(7)[0].notes.create(
	[
		{content: "<ul><li>Discuss how to safely navigate through these sites. Responses could include:</li><li>Never giving out personal information (e.g., home address, school name, telephone number, email or social media account password)</li><li>Not accepting or posting inappropriate pictures</li><li>Not agreeing to meet anyone in person unless safety precautions are in place (e.g., telling parents, having friends or trusted adult come with you, meeting in public place)</li></ul>"}
	]
)

#cyber bullying
deckCB = Deck.find(14)
deckCB.cards.offset(0)[0].notes.create(
  [
    {content: "<ul><li>Encourage the child to tell a trusted adult (e.g., coach, parent, teacher)</li><li>Emphasize telling until someone listens and helps. Role-play this</li><li>Discuss or role-play possible responses -- blocking messages, changing privacy settings, assertively talking to the person, contacting authorities, or other appropriate responses</li></ul>"}
  ]
)
deckCB.cards.offset(1)[0].notes.create(
  [
    {content: "<ul><li>Encourage the child to tell a trusted adult (e.g., coach, parent, teacher)</li><li>Role-play how to respond assertively. Discuss or role-play possible responses -- reporting offensive content to the social media site, assertively talking to the person, or other appropriate responses</li></ul>"}
  ]
)
deckCB.cards.offset(2)[0].notes.create(
  [
    {content: "<ul><li>Discuss with child how this can be another form of bullying</li><li>Role-play how to respond. Role-play responses could include: changing the subject; telling them you do not feel comfortable talking about someone that way</li><li>Role-play how to fend off pressure to participate in this behavior</li></ul>"}
  ]
)
deckCB.cards.offset(3)[0].notes.create(
  [
    {content: "<ul><li>Encourage the child to tell a trusted adult (e.g., coach, parent, teacher)</li><li>Emphasize telling until someone listens and helps. Role-play this</li><li>Role-play possible responses, including: reporting the inappropriate content to the social media site as offensive; assertively talking to the person; or other appropriate responses</li></ul>"}
  ]
)
deckCB.cards.offset(4)[0].notes.create(
  [
    {content: "<ul><li>Discuss appropriate vs. inappropriate internet behavior</li><li>Discuss how this may be another form of bullying (e.g., harassment)</li><li>Role play possible responses: assertively requesting that the person not post on your wall, blocking messages from that person, changing privacy settings, or other assertive responses</li><li>Discuss appropriate occasions to seek help from a trusted adult</li></ul>"}
  ]
)
deckCB.cards.offset(5)[0].notes.create(
  [
    {content: "<ul><li>Role play possible responses that could include: blocking messages from that person, changing privacy settings, requesting that the person not post on your wall, or other assertive responses depending on the severity of the situation</li><li>Discuss appropriate occasions to seek help from a trusted adult</li><li>Discuss importance of seeking help even when bully is not known (e.g., schools may later learn who was responsible; depending on the severity, the ISP or social media site may be able to take action)</li></ul>"}
  ]
)

# Set initial decks
initial_decks = Deck.all
initial_decks.each do |deck|
  deck.original = true
  deck.save
end

# Create Admins

#todo bwj change this before shipping to an MUSC admin
Provider.create({name: "admin", email: "ben@fuzzco.com", password: "password", admin: true})
Provider.create({name: "kyle", email: "kyle@fuzzco.com", password: "password", admin: true})


# Intro Videos

# What do you know?
section = Chapter.find(5).sections.first
section.vimeo_url = "107047094"
section.save

# You are not alone
section = Chapter.find(4).sections.first
section.vimeo_url = '{"boy":"107047399","girl":"107047600"}'
section.save

# Your Body
section = Chapter.find(1).sections.first
section.vimeo_url = "107047665"
section.save

# Relaxation: Breathing
section = Chapter.find(2).sections.first
section.vimeo_url = "107047732"
section.save

# Relaxation: PMR
section = Chapter.find(3).sections.first
section.vimeo_url = "107047855"
section.save

# Affective Regulation
section = Chapter.find(6).sections.first
section.vimeo_url = "107047904"
section.save

# In Vivo Exposure
section = Chapter.find(9).sections.first
section.vimeo_url = "107048495"
section.save

# Trauma Narrative
section = Chapter.find(10).sections.first
section.vimeo_url = "107048533"
section.save

# Enhancing Safety
section = Chapter.find(8).sections.first
section.vimeo_url = "107048620"
section.save

# Enhancing Safety
section = Chapter.find(7).sections.first
section.vimeo_url = "107048349"
section.save



#dev specific 
if Rails.env.development?
  
  Provider.create(
    [
      {
        name: "Chauncey Peppertooth",
        email: "c@pepper.org",
        password: "password"
      }
    ]
  )
  
  provider = Provider.find_by_email('c@pepper.org')
  provider.patients.create(
    [
      {lookup_id: "KRapril13", gender_id: 2, age: 10, race_id: 1, ethnicity_id: 1, trauma1_id: 1},
      {lookup_id: "9boyPA", gender_id: 1, age: 12, race_id: 2, ethnicity_id: 1, trauma1_id: 2},
      {lookup_id: "AAgirlDV", gender_id: 2, age: 14, race_id: 3, ethnicity_id: 1, trauma1_id: 1},
      {lookup_id: "DW10SA", gender_id: 1, is_active: false, age: 16, race_id: 4, ethnicity_id: 1, trauma1_id: 2}
    ]
  )
  
  patient = Patient.find_by_lookup_id('KRapril13')
  
  homework = Homework.find_by_description('Practice positive reinforcement, praise')
  #patient.assignments.create(homework_id: homework.id)
  
  homework = Homework.find_by_description('Practice contingency management skills')
  #patient.assignments.create(homework_id: homework.id)

  homework= Homework.find_by_description('Identify cognitive or thought distortions throughout the week')
  #patient.assignments.create(homework_id: homework.id)
  
end