# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141113144301) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: true do |t|
    t.integer  "homework_id"
    t.integer  "patient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cards", force: true do |t|
    t.integer  "deck_id"
    t.string   "question"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "vimeo_url"
    t.integer  "card_order"
    t.boolean  "active",     default: true
  end

  create_table "chapters", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "prepare_vimeo_url"
    t.integer  "display_order"
    t.integer  "homework_category_id"
  end

  create_table "decks", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.string   "body_class"
    t.integer  "section_id"
    t.integer  "provider_id"
    t.boolean  "original",    default: false
  end

  create_table "degrees", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ethnicities", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "experiences", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "genders", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "homework_categories", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "homeworks", force: true do |t|
    t.string   "description"
    t.integer  "homework_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interactions", force: true do |t|
    t.integer  "section_id"
    t.integer  "patient_id"
    t.string   "data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
    t.string   "data_key"
  end

  create_table "notes", force: true do |t|
    t.integer  "section_id"
    t.integer  "card_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patients", force: true do |t|
    t.integer  "provider_id"
    t.string   "lookup_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "age"
    t.integer  "race_id"
    t.integer  "ethnicity_id"
    t.integer  "gender_id"
    t.boolean  "is_active",    default: true
    t.integer  "trauma1_id"
    t.integer  "trauma2_id"
    t.integer  "trauma3_id"
    t.integer  "trauma4_id"
  end

  create_table "professions", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "providers", force: true do |t|
    t.integer  "degree_id"
    t.integer  "profession_id"
    t.integer  "experience_id"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "admin"
    t.string   "country_code"
    t.string   "state_code"
  end

  add_index "providers", ["email"], name: "index_providers_on_email", unique: true, using: :btree
  add_index "providers", ["reset_password_token"], name: "index_providers_on_reset_password_token", unique: true, using: :btree

  create_table "races", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "section_questions", force: true do |t|
    t.integer  "section_id"
    t.integer  "sequence"
    t.string   "question"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "answer_boys"
    t.integer  "answer_girls"
  end

  add_index "section_questions", ["section_id"], name: "index_section_questions_on_section_id", using: :btree

  create_table "sections", force: true do |t|
    t.integer  "chapter_id"
    t.integer  "sequence"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "vimeo_url"
    t.string   "content_type"
    t.string   "header_text"
    t.string   "body_text"
    t.string   "audio_file"
  end

  create_table "traumas", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
