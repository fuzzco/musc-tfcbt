module SectionsHelper

  def get_vimeo_embed_url(vimeo_id, gender)
  	if vimeo_id != nil && ! vimeo_id.blank?

  		if JSON.is_json?(vimeo_id) && gender
  			vimeo_id = JSON.parse(vimeo_id)[gender]
  		end

  		"//player.vimeo.com/video/#{vimeo_id}?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=0&api=1"
  	end
  end

end
