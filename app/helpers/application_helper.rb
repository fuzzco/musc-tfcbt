module ApplicationHelper

  def validate_active_patient_session
    redirect_to root_url and return unless provider_session.key?(:patient_id)
  
    redirect_to root_url and return if provider_session[:patient_id].nil?
  end
  
end
