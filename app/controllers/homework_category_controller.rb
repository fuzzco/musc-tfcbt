class HomeworkCategoryController < ApplicationController
  before_action :validate_active_patient_session
  
  def list  
    @patient = Patient.find(provider_session[:patient_id])

    load_homework_category_for_patient(params[:id], @patient.id)
    
    respond_to do |format|
      format.js
    end
  end  
end
