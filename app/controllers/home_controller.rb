class HomeController < ApplicationController
  def index
    if provider_signed_in?
      redirect_to current_provider
    else
      redirect_to new_provider_session_path
    end
  end
end
