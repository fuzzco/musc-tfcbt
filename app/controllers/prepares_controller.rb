class PreparesController < ApplicationController

  before_action :authenticate_provider!
  
  #/chapters/:chapter_id/prepares
  def show
  	 @chapter = Chapter.find(params[:id])
  end
  
end
