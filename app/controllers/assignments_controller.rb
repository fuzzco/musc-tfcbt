class AssignmentsController < ApplicationController

  before_action :authenticate_provider!
  before_action :validate_active_patient_session
  before_action :check_authorization

  #/patients/:id/assignments/
  def index
    @patient = Patient.find(params[:patient_id])
    
    load_homework_category_for_patient(1, @patient.id)
  end
  
  private
    
    def check_authorization
      redirect_to current_provider unless current_provider.patients.exists?(params[:patient_id])
    end
end
