class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  include ApplicationHelper
  
  def load_homework_category_for_patient(homework_category_id, patient_id)
    @homeworks = HomeworkCategory.find(homework_category_id).homeworks
    @homeworks = [] unless @homeworks.any?
    @assigned = []  
    @homeworks.each do |homework|
      if homework.assignments.where(:patient_id => patient_id).any?
        @assigned << homework.id
      end
    end
  end
end
