class DecksController < ApplicationController
  
  before_action :authenticate_provider!
  before_action :check_provider, only: [:edit,:update]
  
  def index
    @decks = current_provider.decks
  end

  def edit
  end
  
  def update
    changed_cards_ary = []
    
    # Find all of the changed cards so that we can wipe out the associated provider notes below.
    # There's probably a cleaner, more railsy way to do this but I'm time crunched :(
    # Sorry future dev that's fixing a bug here. I owe you a beer.
    if @deck.original == false    
      @deck.cards.each do |c|
        deck_params["cards_attributes"].each do |cx|
          cxp = cx[1]
          if (cxp["id"].to_i == c.id)
            if cxp["question"] != c.question
              changed_cards_ary << c.id
            end
          end
        end
      end
    end

    if @deck.update_attributes(deck_params)
      changed_cards_ary.each do |c_id|
        c = Card.find(c_id)
        c.notes.each do |n|
          n.delete
        end
      end
      redirect_to decks_path
    else
      render 'edit'
    end
  end
  
  def deck_params
      params.require(:deck).permit(:title, cards_attributes:[:id,:question,:card_order,:active])
  end
  
  def check_admin
    redirect_to root_url unless current_provider.admin?
  end

  def check_provider
    @deck = Deck.find(params[:id])
    redirect_to root_url unless current_provider.admin? || @deck.provider.id == current_provider.id
  end    
  
end
