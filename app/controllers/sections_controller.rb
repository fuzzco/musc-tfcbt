class SectionsController < ApplicationController

  before_action :set_audio_url
  before_action :authenticate_provider!
  before_action :validate_active_patient_session, except: [:create, :destroy]
  before_action :validate_chapter_section_exists, except: [:create]
  before_action :validate_chapter_allows_new_sections, only: [:create, :destroy]
  before_action :check_admin, only: [:create, :destroy]
  
  def create
    current_maximum = @chapter.sections.maximum(:sequence)
    section = @chapter.sections.build
    section.sequence = current_maximum.nil? ? 1 : current_maximum + 1
    section.content_type = 'video'
    section.header_text = "Chapter #{@chapter.display_order} Section #{section.sequence}"
    section.save
    redirect_to edit_chapter_path(@chapter)
  end
  
  def destroy
    @section.destroy
    
    current_sequence = 1
    @chapter.sections.each do |s|
      if s.sequence != current_sequence
        s.sequence = current_sequence
        s.save
      end
      current_sequence += 1
    end
    
    redirect_to :back
  end
  
  #/chapters/:chapter_id/sections/:id/
  #note that :id is the 'sequence' of the section within the chapter 
  def show    
    
    @patient = Patient.find(provider_session[:patient_id])
    @section_count = @chapter.sections.count
    @section_current = @section.sequence
    @gender_pronoun_plural = @patient.gender.id == 1 ? "boys" : "girls"
    
    case @chapter.id
    when 1
      case @section.sequence
      when 1
        render 'chapter-01-video'
      when 2
        if @patient.gender.description == "Male"
          render 'chapter-01-boy-front'
        else
          render 'chapter-01-girl-front'
        end
      when 3
        if @patient.gender.description == "Male"
          render 'chapter-01-boy-back'
        else
          render 'chapter-01-girl-back'
        end
      when 4
        if @patient.gender.description == "Male"
          render 'chapter-01-girl-front'
        else
          render 'chapter-01-boy-front'
        end
      when 5
        if @patient.gender.description == "Male"
          render 'chapter-01-girl-back'
        else
          render 'chapter-01-boy-back'
        end
      end

    when 2
      case @section.sequence
      when 1
        render 'chapter-02-video'
      when 2
        @next_session_sequence 
        render 'chapter-02-fast'
      when 3
        render 'chapter-02-slow'
      end
    when 3
      case @section.sequence
      when 1
        render 'chapter-03-video'
      when 2
        if @patient.gender.description == "Male"
          render 'chapter-03-boy'
        else
          render 'chapter-03-girl'
        end
      end
    when 4
      case @section.sequence
      when 1
        render 'chapter-04-video'
      when 2
        if @patient.gender.description == "Male"
          render 'chapter-04-boys'
        else
          render 'chapter-04-girls'
        end
      when 3
        if @patient.gender.description == "Male"
          render 'chapter-04-boys'
        else
          render 'chapter-04-girls'
        end
      end
    when 5
      case @section.sequence
      when 1
        render 'chapter-05-video'
      when 2
        render 'chapter-05-decks'
      when 3
        if params[:deck_id]
        	@deck = Deck.find(params[:deck_id])
        	# associate the interaction with the deck selection screen (@section.id - 1)
			    # originally Interaction.create(section_id: @section.id - 1, patient: @patient, description: "Response", data_key: params[:deck_id], data: 1)
          Interaction.create(section_id: @section.id - 1, patient: @patient, description: "Response", data_key: "deck_index", data: params[:deck_index])
          render 'chapter-05-slides'
		else
			redirect_to patient_assignments_path(@patient)
        end
        
      end
    when 6
      case @section.sequence
      when 1
        render 'chapter-06-video'
      when 2
        render 'chapter-06-writing-feelings'
      when 3
        if @patient.age > 12 then
          render 'chapter-06-measure-feelings-over-12' 
        else
          render 'chapter-06-measure-feelings-under-12'
        end
      when 4
        if @patient.age < 13 || @patient.age > 15
          render 'chapter-06-charades'
        else
          redirect_to chapter_section_path(@chapter.id, @section.sequence + 1)
        end
      when 5
        render 'chapter-06-writing-upsetting-feelings'
      when 6
        render 'chapter-06-writing-traumatic-feelings'
      end
    when 7
      @section_count = 9
      @trackTitles = [
        'My friends are laughing at me.',
        'My friends must have told a funny joke.',
        'I am being left out.'
      ]
      @videos = [
        '98067646',
        '98067515',
        '98067522'
      ]
      @tracks = [
        [0,2,1],
        [1,0,2],
        [2,0,1]
      ]
      case @section.sequence
      when 1
        @section_current = 1
        render 'chapter-07-video'
      when 2
        @section_current = 2
        render 'chapter-07-matching'
      when 3
        @section_current = 2
        render 'chapter-07-matching2'
      when 4
        @section_current = 3
        render 'chapter-07-video2'
      when 5
        @section_current = 4
        render 'chapter-07-best-answer'
      when 6
        @section_current = 5
        render 'chapter-07-multiple-choice'
      when 7
        @section_current = 5
        render 'chapter-07-video3'
      when 8
        @section_current = 5
        render 'chapter-07-multiple-choice2'

        #new sections
      when 9
        @section_current = 5
        render 'chapter-07-video4'
      when 10
        @section_current = 5
        render 'chapter-07-multiple-choice3'
      when 11
        @section_current = 5
        render 'chapter-07-video5'


      when 12
        @section_current = 6
        render 'chapter-07-test'
      when 13
        @section_current = 6
        render 'chapter-07-test-b'
      when 14
        @section_current = 6
        render 'chapter-07-test-c'
      when 15
        @section_current = 6
        render 'chapter-07-test-d'
      when 16
        @section_current = 7
        render 'chapter-07-test-2'
      when 17
        @section_current = 7
        render 'chapter-07-test-2-b'
      when 18
        @section_current = 7
        render 'chapter-07-test-2-c'
      when 19
        @section_current = 7
        render 'chapter-07-test-2-d'
      when 20
        @section_current = 8
        render 'chapter-07-test-3'
      when 21
        @section_current = 8
        render 'chapter-07-test-3-b'
      when 22
        @section_current = 8
        render 'chapter-07-test-3-c'
      when 23
        @section_current = 8
        render 'chapter-07-test-3-d'
      when 24
        @section_current = 8
        render 'chapter-07-video6'
      when 25
        @section_current = 9
        render 'chapter-07-test-4'
      when 26
        @section_current = 9
        render 'chapter-07-test-4-b'
      when 27
        @section_current = 9
        render 'chapter-07-test-4-c'
      when 28
        @section_current = 9
        render 'chapter-07-test-4-d'
      when 29
        @section_current = 9
        render 'chapter-07-test-4-e'
      end
    when 8
      case @section.sequence
      when 1
        render 'chapter-08-video'
      when 2
        render 'chapter-08-decks'
      when 3
      	if params[:deck_id]
	        @deck = Deck.find(params[:deck_id])
	        
	        # associate the interaction with the deck selection screen (@section.id - 1)
	        #Interaction.create(section_id: @section.id - 1, patient: @patient, description: "Response", data_key: params[:deck_id], data: 1)
          Interaction.create(section_id: @section.id - 1, patient: @patient, description: "Response", data_key: "deck_index", data: params[:deck_index])
	        
	        render 'chapter-08-slides'
        
        else
			redirect_to patient_assignments_path(@patient)
        end
        
      end
    when 9

      # Set gender specific details
      if @patient.gender.description == "Male"
        @name = "John"
        @pronoun = "him"
        @possessive = "his"
        @direct = "he"
        @gender = "boy"
      elsif @patient.gender.description == "Female"
        @name = "Jane"
        @pronoun = "her"
        @possessive = "her"
        @direct = "she"
        @gender = "girl"
      end

      # Set audio files
      case @patient.trauma1_id
      when 1
        @section.audio_file = "in-vivo-exposure/#{@gender}/sexual-abuse/#{@section.audio_file}"
      when 2
        @section.audio_file = "in-vivo-exposure/#{@gender}/physical-abuse/#{@section.audio_file}"     
      when 3
        @section.audio_file = "in-vivo-exposure/#{@gender}/accident/#{@section.audio_file}"
      when 4
        @section.audio_file = "in-vivo-exposure/#{@gender}/domestic-violence/#{@section.audio_file}"
      when 5, 7
        @section.audio_file = "in-vivo-exposure/#{@gender}/bullying/#{@section.audio_file}"
      when 6, 8
        @section.audio_file = "in-vivo-exposure/#{@gender}/disaster/#{@section.audio_file}"
      end


      @section_count = 7
      case @section.sequence
      when 1
        @section_current = 1
        render 'chapter-09-video'
      when 2
        @section_current = 2
        @section.audio_file = "in-vivo-exposure/#{@gender}/beginning_narration.mp3"
        render 'chapter-09-audio-test'
      when 3, 4, 5, 6, 7, 8
        @section_current = 3
        case @patient.trauma1_id
        when 1
          render 'chapter-09-audio-a' #sexual abuse
        when 2
          render 'chapter-09-audio-c' #physical abuse          
        when 3
          render 'chapter-09-audio-b' #car crash / accident
        when 4
          render 'chapter-09-audio-e' #domestic violence
        when 5, 7
          render 'chapter-09-audio-f' #school violence / bullying
        when 6, 8
          render 'chapter-09-audio-d' #hurricane / death of a loved one
        when 9
          #other
        end
      when 9
        @section_current = 4
        render 'chapter-09-list-intro'
      when 10
        @section_current = 5
        case @patient.trauma1_id
        when 1
          render 'chapter-09-list-a' #sexual abuse
        when 2
          render 'chapter-09-list-c' #physical abuse          
        when 3
          render 'chapter-09-list-b' #car crash
        when 4
          render 'chapter-09-list-e' #domestic violence
        when 5, 7
          render 'chapter-09-list-f' #school violence / bullying
        when 6, 8
          render 'chapter-09-list-d' #hurricane / death of a loved one
        when 9
          #other
        end
      when 11
        @section_current = 5
        case @patient.trauma1_id
        when 1
          render 'chapter-09-list-a2' #sexual abuse
        when 2
          render 'chapter-09-list-c2' #physical abuse          
        when 3
          render 'chapter-09-list-b2' #car crash
        when 4
          render 'chapter-09-list-e2' #domestic violence
        when 5, 7
          render 'chapter-09-list-f2' #school violence / bullying
        when 6, 8
          render 'chapter-09-list-d2' #hurricane / death of a loved one
        when 9
          #other
        end
      when 12
        @section_current = 5
        case @patient.trauma1_id
        when 1
          render 'chapter-09-list-a3' #sexual abuse
        when 2
          render 'chapter-09-list-c3' #physical abuse          
        when 3
          render 'chapter-09-list-b3' #car crash
        when 4
          render 'chapter-09-list-e3' #domestic violence
        when 5, 7
          render 'chapter-09-list-f3' #school violence / bullying
        when 6, 8
          render 'chapter-09-list-d3' #hurricane / death of a loved one
        when 9
          #other
        end
      when 13
        @section_current = 5
        case @patient.trauma1_id
        when 1
          render 'chapter-09-list-a4' #sexual abuse
        when 2
          render 'chapter-09-list-c4' #physical abuse          
        when 3
          render 'chapter-09-list-b4' #car crash
        when 4
          render 'chapter-09-list-e4' #domestic violence
        when 5, 7
          render 'chapter-09-list-f4' #school violence / bullying
        when 6, 8
          render 'chapter-09-list-d4' #hurricane / death of a loved one
        when 9
          #other
        end
      when 14
        @section_current = 6
        case @patient.trauma1_id
        when 1
          render 'chapter-09-ladder-a' #sexual abuse
        when 2
          render 'chapter-09-ladder-c' #physical abuse          
        when 3
          render 'chapter-09-ladder-b' #car crash
        when 4
          render 'chapter-09-ladder-e' #domestic violence
        when 5, 7
          render 'chapter-09-ladder-f' #school violence / bullying
        when 6, 8
          render 'chapter-09-ladder-d' #hurricane / death of a loved one
        when 9
          #other
        end
      when 15
        @section_current = 7
        @section.audio_file = "in-vivo-exposure/#{@gender}/final_narration.mp3"
        render 'chapter-09-own-list'
      end
    when 10
      case @section.sequence
      when 1
        render 'chapter-10-video'
      when 2
        render 'chapter-10-paper'
      end
    when 11
      case @section.sequence
      when 1
        render 'chapter-11-index'
      when 2
        @deck = Deck.find(params[:deck_id])
        
        Interaction.create(section_id: @section.id - 1, patient: @patient, description: "Response", data_key: "deck_index", data: params[:deck_index])
        
        render 'chapter-11-slides'
      end
    else
      redirect_to chapters_url
    end
  
  end
  
  #put /chapters/:chapter_id/sections/:section_id/interaction
  def interaction
    @patient = Patient.find(provider_session[:patient_id])
    
    Interaction.create(section: @section, patient: @patient, description: "Response", data_key: params[:data_key], data: params[:data])
    
    # sends an HTTP 201 with no body
    head :created
  end
  
  #put /chapters/:chapter_id/sections/:section_id/finish
  def finish
    @patient = Patient.find(provider_session[:patient_id])
    
    Interaction.create(section: @section, patient: @patient, description: "Visit")
    
    #add any extra params that sections need here
    filtered_params = params.slice(:deck_id, :deck_index)
    
    #hack alert
    if @chapter.id == 9 && @section.sequence.between?(3, 8)
      redirect_to chapter_section_path(@chapter.id, 9, filtered_params) and return
    end
    
    if Section.exists?(:chapter_id => @chapter.id, :sequence => @section.sequence + 1)
      redirect_to chapter_section_path(@chapter.id, @section.sequence + 1, filtered_params)
    else
      redirect_to patient_assignments_path(@patient, :chapter_id => @chapter.id)
    end
  end

  private
    
    def validate_chapter_section_exists
      @chapter = Chapter.find_by_id(params[:chapter_id])
      redirect_to chapters_url if @chapter.nil?
      
      @section = @chapter.sections.find_by_sequence(params[:id])
      redirect_to chapters_url if @section.nil?
    end
    
    def validate_chapter_allows_new_sections
      @chapter = Chapter.find_by_id(params[:chapter_id])
      
      if @chapter.id == 11
        return true
      end
      
      return false
    end
    
    def check_admin
      redirect_to root_url unless current_provider.admin?
    end

    def set_audio_url
      @audioUrl ||= Note.find(1).content
    end
  
end
