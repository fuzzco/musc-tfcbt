require 'csv'

class ProvidersController < ApplicationController
  
  before_action :authenticate_provider!
  before_action :check_authorization
  before_action :check_admin, except: [:show, :prepare]
  
  def index
    @providers = Provider.all.where.not(id: current_provider.id)
  end
  
  def show
    @provider = Provider.find(params[:id])
    
    provider_session[:patient_id] = nil
  end
  
  def prepare
    @provider = Provider.find(params[:id])
    provider_session[:patient_id] = nil
  end
  
  def new
    @provider = Provider.new()
  end
  
  def create
    @provider = Provider.new(provider_params)
    
    if @provider.save
      redirect_to providers_path
    else
      flash[:error] = @provider.errors.full_messages
      render 'new'
    end
  end
  
  def edit
    @provider = Provider.find(params[:id])
  end
  
  def update
    @provider = Provider.find(params[:id])
    
    if @provider.update_without_password(provider_params)
      redirect_to providers_path
    else
      render 'edit'
    end
  end
  
  def destroy
    Provider.destroy(params[:id])
    redirect_to :back
  end
  
  def export
    @csv = ''
    
    @csv << CSV.generate_line([
      'provider id', 
      'provider name',
      'provider e-mail',
      'provider degree',
      'provider country',
      'provider state (if US)',
      'provider profession',
      'provider years of experience',
      'patient id',
      'patient gender',
      'patient age',
      'patient race',
      'patient ethnicity',
      'patient index trauma',
      'patient trauma #2',
      'patient trauma #3',
      'patient trauma #4',
      'chapter 1 use',
      'chapter 1 first date',
      'chapter 1 last date',
      'chapter 1 deck 1',
      'chapter 1 deck 2',
      'chapter 1 deck 3',
      'chapter 1 deck 4',
      'chapter 1 deck 5',
      'chapter 1 deck 6',
      'chapter 1 deck 7',
      'chapter 1 deck 8',
      'chapter 2 use',
      'chapter 2 first date',
      'chapter 2 last date',
      'chapter 2 dropdown item 1 answer',
      'chapter 2 dropdown item 2 answer',
      'chapter 2 dropdown item 3 answer',
      'chapter 2 dropdown item 4 answer',
      'chapter 2 dropdown item 5 answer',
      'chapter 2 dropdown item 6 answer',
      'chapter 2 dropdown item 7 answer',
      'chapter 2 dropdown item 8 answer',
      'chapter 2 dropdown item 9 answer',
      'chapter 2 dropdown item 10 answer',
      'chapter 2 dropdown item 11 answer',      
      'chapter 3 use',
      'chapter 3 first date',
      'chapter 3 last date',
      'chapter 3 complete front (boy)',
      'chapter 3 complete back (boy)',
      'chapter 3 complete front (girl)',
      'chapter 3 complete back (girl)',
      'chapter 4 use',
      'chapter 4 first date',
      'chapter 4 last date',
      'chapter 4 deck 1',
      'chapter 4 deck 2',
      'chapter 4 deck 3',
      'chapter 4 deck 4',
      'chapter 4 deck 5',
      'chapter 5 use',
      'chapter 5 first date',
      'chapter 5 last date',
      'chapter 6 use',
      'chapter 6 first date',
      'chapter 6 last date',
      'chapter 7 use',
      'chapter 7 first date',
      'chapter 7 last date',
      'chapter 8 use',
      'chapter 8 first date',
      'chapter 8 last date',
      'chapter 8 complete',
      'chapter 9 use',
      'chapter 9 first date',
      'chapter 9 last date',
      'chapter 10 use',
      'chapter 10 first date',
      'chapter 10 last date',
      'chapter 11 use',
      'chapter 11 first date',
      'chapter 11 last date',
      'chapter 11 deck 1',
      'chapter 11 deck 2',
      'chapter 11 deck 3',
      'chapter 11 deck 4',
      'chapter 11 deck 5',
      'chapter 11 deck 6'
    ])
    
    chapter_first_section = Hash[Section.where(sequence: 1).map{|s| [s.chapter.display_order, s.id]}]
    cognitive_coping_chapter = Chapter.where(display_order: 8).first
    last_coping_section = Section.where(chapter_id: cognitive_coping_chapter.id).order('sequence DESC').first
    
    Provider.all.each do |provider|
      provider.patients.each do |patient|
        @csv << CSV.generate_line([
          provider.id, 
          provider.name, 
          provider.email,
          provider.degree.try(:description),
          provider.country_code,
          provider.state_code,
          provider.profession.try(:description),
          provider.experience.try(:description),
          patient.lookup_id,
          patient.gender.try(:description),
          patient.age,
          patient.race.try(:description),
          patient.ethnicity.try(:description),
          patient.trauma1.try(:description),
          patient.trauma2.try(:description),
          patient.trauma3.try(:description),
          patient.trauma4.try(:description),
          patient.interactions.where(section_id: chapter_first_section[1], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[1]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[1]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[1] + 1, description: 'Response', data: '1', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[1] + 1, description: 'Response', data: '2', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[1] + 1, description: 'Response', data: '3', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[1] + 1, description: 'Response', data: '4', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[1] + 1, description: 'Response', data: '5', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[1] + 1, description: 'Response', data: '6', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[1] + 1, description: 'Response', data: '7', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[1] + 1, description: 'Response', data: '8', data_key: 'deck_index').any? ? 'selected' : 'not selected',        
          patient.interactions.where(section_id: chapter_first_section[2], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[2]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[2]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '1').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '2').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '3').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '4').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '5').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '6').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '7').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '8').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '9').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '10').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[2] + 1, description: 'Response', data_key: '11').order('created_at DESC').first.try(:data),
          patient.interactions.where(section_id: chapter_first_section[3], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[3]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[3]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[3] + (patient.gender_id === 1 ? 1 : 3), description: 'Visit').any? ? 1 : 0,
          patient.interactions.where(section_id: chapter_first_section[3] + (patient.gender_id === 1 ? 2 : 4), description: 'Visit').any? ? 1 : 0,
          patient.interactions.where(section_id: chapter_first_section[3] + (patient.gender_id === 1 ? 3 : 1), description: 'Visit').any? ? 1 : 0,
          patient.interactions.where(section_id: chapter_first_section[3] + (patient.gender_id === 1 ? 4 : 2), description: 'Visit').any? ? 1 : 0,
          patient.interactions.where(section_id: chapter_first_section[4], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[4]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[4]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[4], description: 'Response', data: '1', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[4], description: 'Response', data: '2', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[4], description: 'Response', data: '3', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[4], description: 'Response', data: '4', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[4], description: 'Response', data: '5', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[5], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[5]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[5]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[6], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[6]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[6]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[7], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[7]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[7]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[8], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[8]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[8]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: last_coping_section.id).any? ? 1 : 0,
          patient.interactions.where(section_id: chapter_first_section[9], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[9]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[9]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[10], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[10]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[10]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[11], description: 'Visit').count,
          patient.interactions.where(section_id: chapter_first_section[11]).order('created_at ASC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[11]).order('created_at DESC').first.try(:created_at),
          patient.interactions.where(section_id: chapter_first_section[11] + 1, description: 'Response', data: '1', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[11] + 1, description: 'Response', data: '2', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[11] + 1, description: 'Response', data: '3', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[11] + 1, description: 'Response', data: '4', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[11] + 1, description: 'Response', data: '5', data_key: 'deck_index').any? ? 'selected' : 'not selected',
          patient.interactions.where(section_id: chapter_first_section[11] + 1, description: 'Response', data: '6', data_key: 'deck_index').any? ? 'selected' : 'not selected',
        ])
      end
    end
    
    #render text: @csv, :content_type => 'application/csv' <-- can't suggest filename
    send_data @csv, :type => 'application/csv', :filename => "TFCBT.#{DateTime.now.to_s.gsub(/[^A-Za-z0-9]/, '')}.csv" 
        
  end
  
  private
    
    def check_authorization
      redirect_to root_url unless (current_provider.admin? || params[:id].to_s == current_provider.id.to_s)
    end
    
    def check_admin
      redirect_to root_url unless current_provider.admin?
    end
    
    def provider_params
      params.require(:provider).permit(:name, :email, :password, :degree_id, :experience_id, :profession_id, :country_code, :state_code)
    end
  
end
