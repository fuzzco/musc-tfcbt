class ChaptersController < ApplicationController

  before_action :authenticate_provider!
  before_action :validate_active_patient_session, except: [:edit, :update]

  def index
    @patient = Patient.find(provider_session[:patient_id])
    render 'index'
  end
  
  def edit
    @chapter = Chapter.find(params[:id])
  end
  
  def update
  	@chapter = Chapter.find(params[:id])

  	@chapter.name = params["chapter"]["name"]
  	@chapter.prepare_vimeo_url = params["chapter_prepare_vimeo_url"]
	
  	@chapter.save
	
  	first_section = @chapter.sections.first
    unless first_section.nil?
    	first_section.vimeo_url = params["chapter_vimeo_url"]
    	first_section.save
    end
    
    @chapter.sections.all.each do |section|
      if params.key?("section_#{section.id}_header_text")
        section.header_text = params["section_#{section.id}_header_text"]
        section.save
      end
    
      if params.key?("section_#{section.id}_body_text")
        section.body_text = params["section_#{section.id}_body_text"]
        section.save
      end
      
      if params.key?("section_#{section.id}_audio_file")
        section.audio_file = params["section_#{section.id}_audio_file"]
        section.save
      end
      
      if params.key?("section_#{section.id}_vimeo_url")
        section.vimeo_url = params["section_#{section.id}_vimeo_url"]
        section.save
      end
      
      section.notes.all.each_with_index do |note, index|
      	if params.key?("section_#{section.id}_note_#{index + 1}")
      		note.content = params["section_#{section.id}_note_#{index + 1}"]
      		note.save
      	end
      end
      
      section.section_questions.all.each_with_index do |question, index|
        if params.key?("section_#{section.id}_question_#{question.sequence}")
          question.question = params["section_#{section.id}_question_#{question.sequence}"]
          question.answer_boys = params["section_#{section.id}_question_#{question.sequence}_answer_boys"]
          question.answer_girls = params["section_#{section.id}_question_#{question.sequence}_answer_girls"]
          question.save
        end
      end
      
    end
    
    redirect_to edit_content_path
  end
  
  def show
    @chapter = Chapter.find(params[:id])
    @section = @chapter.sections.order(:sequence).first
    redirect_to chapter_section_path(@chapter, @section.sequence)
  end
  
  private
    
    def chapter_params
      params.require(:chapter).permit(:name, :prepare_vimeo_url)
    end

end
