class PatientsController < ApplicationController

  before_action :authenticate_provider!
  before_action :authorize_patient_for_provider, except: [:new, :create]

  #/providers/:id/patients/new
  def new
    @provider = Provider.find(params[:provider_id])
    @patient = @provider.patients.build
  end
  
  #/providers/:id/patients
  def create
    @provider = Provider.find(params[:provider_id])
    @patient = @provider.patients.build(patient_params)
    
    if @patient.save
      redirect_to @patient
    else
      render 'new'
    end
  end
  
  #/patients/:id/
  def show
    @patient = Patient.find(params[:id])
    
    @assignments = Homework.joins([:patients, :homework_category]).where("patients.id" => @patient.id).order("homework_categories.id ASC")
  end
  
  #/patients/:id/edit
  def edit
    @patient = Patient.find(params[:id])
  end
  
  #/patients/:id/
  def update
    @patient = Patient.find(params[:id])
    if @patient.update_attributes(patient_params)
      redirect_to @patient
    else
      render 'edit'
    end
  end
  
  #/patients/:id/activate
  def activate
    @patient = Patient.find(params[:id])
    @patient.is_active = true
    @patient.save
    redirect_to @patient
  end
  
  #/patients/:id/deactivate
  def deactivate
    @patient = Patient.find(params[:id])
    @patient.is_active = false
    @patient.save
    redirect_to @patient
  end
  
  #/patients/:id/start
  def start_session
    @patient = Patient.find(params[:id])
    @patient.assignments.destroy_all
    provider_session[:patient_id] = @patient.id
    redirect_to chapters_path
  end
  
  #/patients/:id/assign/:homework_id
  def assign_homework
    @patient = Patient.find(params[:id])
    @homework = Homework.find(params[:homework_id])
    
    # if any existing assignments exist exist, destroy them; otherwise, create one
    if Assignment.where(:patient_id => @patient.id, :homework_id => @homework.id).any?
      Assignment.delete_all(:patient_id => @patient.id, :homework_id => @homework.id)
    else
      Assignment.create(patient_id: @patient.id, homework_id: @homework.id)
    end
    
    load_homework_category_for_patient(@homework.homework_category_id, @patient.id)
    
    respond_to do |format|
      format.js {render 'homework_category/list'}
    end
  end
  
  private
  
    def patient_params
      params.require(:patient).permit(:lookup_id, :age, :race_id, :ethnicity_id, :gender_id, :trauma1_id, :trauma2_id, :trauma3_id, :trauma4_id)
    end
    
    def authorize_patient_for_provider
      redirect_to current_provider unless current_provider.patients.exists?(params[:id])
    end
    
end
