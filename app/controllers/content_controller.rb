class ContentController < ApplicationController
  before_action :authenticate_provider!
  before_action :check_admin
  
  def edit
    @chapters = Chapter.all.order("id ASC")
    @audioUrl = Note.find(1).content
  end
  
  def update
    Chapter.all.each do |chapter|
      chapter.name = params["chapter_#{chapter.id}_name"]
      chapter.prepare_vimeo_url = params["chapter_prepare_vimeo_url"]
      
      first_section = chapter.sections.first
      first_section.vimeo_url = params["chapter_#{chapter.id}_vimeo_url"]
      first_section.save
      
      chapter.save
    end
    
    Section.all.each do |section|
      if params.key?("section_#{section.id}_header_text")
        section.header_text = params["section_#{section.id}_header_text"]
        section.save
      end
      
      if params.key?("section_#{section.id}_body_text")
        section.body_text = params["section_#{section.id}_body_text"]
        section.save
      end
    end
    
    redirect_to root_url
  end

  def update_audio_url
    @chapters = Chapter.all.order("id ASC")
    @note = Note.find(1)
    @note.update_attribute("content", params[:audio_url])
    @audioUrl = @note.content

    render "edit"
  end
  
  def check_admin
    redirect_to root_url unless current_provider.admin?
  end
  
end
