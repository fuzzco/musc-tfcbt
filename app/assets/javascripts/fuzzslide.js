// Slideshow
(function($){
    $.fn.fuzzslide = function(options) {
    	this.each(function() {
			var $this = $(this);
			
			// Defaults and Options
			$this.data = $this.data('fuzzslide');
			$this.data = $.extend({
				slides : $('<div/>'),
				previous : $('<div/>'),
				next : $('<div/>'),
				copies : $('<div/>'),
				wrap : $('<div/>'),
				picks : $('<div/>'),
				slide_time : 5000,
				slide_type : 'fade',
				autoplay : '',
				center : ''
		    }, options);
			
			// Slideshow Setup
			//if ($this.data.slides.length <= 1) { return; }
			
			if ($this.data.autoplay != '') {
				$this.data.interval = setInterval(slide_next, $this.data.slide_time);
			}
			
			$this.data.previous.on('click touchstart', function() {
				clearInterval($this.data.interval);
				slide_previous();
				return false;
			});
			$this.data.next.on('click touchstart', function() {
				clearInterval($this.data.interval);
				slide_next();
				return false;
			});
			$this.data.picks.each(function(index) {
				$(this).click(slide_pick);
			});
			
			$this.addClass('current');
			$this.data.slides.hide().fadeIn();
			$this.data.slides.removeClass('current').eq(0).addClass('current');
			$this.data.picks.removeClass('current').eq(0).addClass('current');
			$this.data.copies.removeClass('current').eq(0).fadeIn().addClass('current');
			
			// Slide Functions
			function slide_previous(number) {

				if ($this.data.slides.length) {
					$toSlide = $this.data.slides.eq(0).width() + parseInt($this.data.slides.eq(0).css('padding-left'));	
				} else {
					$toSlide = $this.width() / 2;
				}

				if (typeof number == "undefined") number = 1;
	   			$toSlide = $toSlide * number;
				
				if (($this.data.wrap.offset().left + $toSlide) < 1) {
					$this.data.wrap.animate({ left : '+='+$toSlide }, 1000, 'easeInOutExpo');
				} else {
					$this.data.wrap.animate({ left : 0 }, 1000, 'easeInOutExpo');
				}
				
				var current = $this.data.slides.filter('.current');
				var current_index = $this.data.slides.index(current);
				$(current).removeClass('current');
				$this.data.picks.filter('.current').removeClass('current');
				if (current_index == 0) {
					$this.data.slides.first().addClass('current');
					$this.data.picks.first().addClass('current');
				}
				else {
					$this.data.slides.eq(current_index - number).addClass('current');
					$this.data.picks.eq(current_index - number).addClass('current');
				}
	   		}
	   		function slide_next(number) {	   		
	   			var current = $this.data.slides.filter('.current');
				var current_index = $this.data.slides.index(current);
				
	   			if ($this.data.slides.length) {
	   				$toSlide = $this.data.slides.eq(0).width() + parseInt($this.data.slides.eq(0).css('padding-left'));
	   			} else {
	   				$toSlide = $this.width() / 2;
	   			}

	   			if (typeof number == "undefined") number = 1;
	   			$toSlide = $toSlide * number;

	   			
	   			$maxLeft = -($this.data.wrap.width() - $(window).width()) + 168;
	   			
	   			if ((current_index + 1) != $this.data.slides.length) {
	   				$this.data.wrap.animate({ left : '-='+$toSlide }, 1000, 'easeInOutExpo');
	   			}
				
				$(current).removeClass('current');
				$this.data.picks.filter('.current').removeClass('current');
				if (current_index == $this.data.slides.length - 1) {
					$this.data.slides.last().addClass('current');
					$this.data.picks.last().addClass('current');
				}
				else {
					$this.data.slides.eq(current_index + number).addClass('current');
					$this.data.picks.eq(current_index + number).addClass('current');
				}
	   		}
	   		function slide_pick() {
	   			var current = $this.data.slides.filter('.current');
				var current_index = $this.data.slides.index(current);
				var pick_index = $(this).index();

				if (pick_index < current_index) {
					slide_previous( current_index - pick_index );
				} else if (pick_index > current_index) {
					slide_next( pick_index - current_index );
				}

	   			return false;
	   		}
   		});
   		return this;
    };
})(jQuery);