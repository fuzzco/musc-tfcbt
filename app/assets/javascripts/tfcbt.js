$(document).ready(function(){

	$('.button').bind(
		'touchmove',
		function(e) {
			e.preventDefault();
		}
	);

	// Delay Loading of Images
	$('img').each(function () {
		var source = $(this).attr('data-src');
		var hdSource = $(this).attr('data-src-retina');
		if (hdSource != undefined && isRetina) {
			$(this).attr('src', hdSource).show();
		}
		else if (source != null) {
			$(this).attr('src', source).show();
		}
	});

	// Add Device Class to Body
	if (isMobile) {
		$('body').addClass('mobile');
	} else {
		$('body').addClass('desktop');
	}

	// Chapter Specific Logic
	if ($('body').is('.chapter-01')) {
		Chapter01();
	} else if ($('body').is('.chapter-02')) {
		Chapter02();
	} else if ($('body').is('.chapter-03')) {
		Chapter03();
	} else if ($('body').is('.chapter-04')) {
		Chapter04();
	} else if ($('body').is('.chapter-05')) {
		Chapter05();
	} else if ($('body').is('.chapter-06.writing')) {
		Chapter06Writing();
	} else if ($('body').is('.chapter-06.charades')) {
		Chapter06Charades();
	} else if ($('body').is('.chapter-07')) {
		Chapter07();
	} else if ($('body').is('.chapter-08')) {
		Chapter08();
	} else if ($('body').is('.chapter-09.list.exercise')) {
		Chapter09ListExercise();
	} else if ($('body').is('.chapter-09.ladder')) {
		Chapter09LadderExercise();
	} else if ($('body').is('.chapter-11.videos')) {
		Chapter11Videos();
	} else if ($('body').is('.password-recovery')) {
		PasswordRecovery();
	} else if ($('body').is('.patient-homework')) {
		PatientHomework();
	} else if ($('body').is('.new-patient')) {
		NewPatient();
	} else if ($('body').is('.provider-home')) {
		ProviderHome();
	} else if ($('body').is('.provider-info.edit')) {
		ProviderEdit();
	} else if ($('body').is('.content-edit')) {
		ContentEdit();
	} else if ($('body').is('.edit-deck')) {
		EditDeck();
	}

	if ($('body').is('.provider-info')) {
		ProviderInfo();
	}

	if ($('.audio-file[data-source]').length > 0) {
		Audio();
	}

	// Menu
	$(document).on('touchstart', '.mobile .site-header li.menu > a', function(event) {
		event.preventDefault();
		$('.sub-navigation').toggle();
	});

	$(document).on('click', '.site-header li.menu > a', function(event) {
		event.preventDefault();
	});

	$(document).on('click', '.sub-navigation a', function(event) {
		event.stopPropagation();
	});

	// Selector
	$(document).on('click touchstart', '.selector-trigger', function(event) {
		event.preventDefault();
		$('.selector-bottom', $(this).parents('.selector')).toggle();
	});

	$(document).on('click touchstart', '.selector-bottom a', function(event) {
		event.preventDefault();
		$('.selector-bottom a.selected').removeClass('selected');
		$(this).addClass('selected');
		$('.selector-trigger .right', $(this).parents('.selector')).html($('.right', this).html()).addClass('selected');
		$('.selector-bottom', $(this).parents('.selector')).toggle();
	});

	// Coins
	$(document).on('click touchstart', '.coin', function(event) {
		event.preventDefault();
		if ($('body').is('.coin-operated')) {
			$('.heads, .tails', this).toggle();
			$(this).toggleClass('selected');
		}
	});

	// Score Buttons
	$(document).on('click touchstart', '.button.minus, .button.plus', function(event) {
		event.preventDefault();
		if ($(this).is('.minus')) {
			$(this).siblings('.value').html(parseInt($(this).siblings('.value').html()) - 1);
		} else {
			$(this).siblings('.value').html(parseInt($(this).siblings('.value').html()) + 1);
		}
	});

	// Styled Selectbox
	$('select').each(function() {
		$(this).wrap('<span class="selectbox" style="display:inline-block;position:relative" />');
		$(this).after('<span class="arrow" />');
		$(this).after('<span class="selected-value" />');
		$(this).css({ 'z-index' : 2, 'opacity' : 0 }).show();
	});

	$('.selectbox').each(function(){
		var selection = $('option:selected', this).text();
		var defaultSelection = $(this).children('select').attr('data-default');

		if (selection == '') {
			$(this).children('span.selected-value').text(defaultSelection);
		} else {
			$(this).children('span.selected-value').text(selection);
		}
	});

	$('select').focus(function() {
		$(this).parent('.selectbox').addClass('active');
	});
	$('select').blur(function() {
		$(this).parent('.selectbox').removeClass('active');
	});

	$('.selectbox select').change(function(){
		var newSelection = $('option:selected', this).text();
		var defaultSelection = $(this).attr('data-default');

		if (newSelection == '') {
			$(this).siblings('span.selected-value').text(defaultSelection);
		} else {
			$(this).siblings('span.selected-value').text(newSelection);
		}
		$(this).parent('.selectbox').removeClass('active');
		$(this).parent('.selectbox').removeClass('error');
		$(this).blur();
	});

	// Slideshows
	$('.slideshow').each(function() {
		$(this).fuzzslide({
			slides : $('.slideshow .slide'),
			previous : $('.slideshow-controls .previous'),
			next : $('.slideshow-controls .next'),
			wrap : $('.slide-wrap'),
			picks : $('.slideshow-controls .pick')
		});
	});

	// Keyboard Support
	$(document).keydown(function(e)	{
		switch(e.keyCode) {
			case 37 : $('.slideshow-controls .previous').click();
				break;
			case 39 : $('.slideshow-controls .next').click();
				break;
		}
	});

	// Stage Sections
	$(document).on('click', '.stage-section .continue', function(event) {
		event.preventDefault();
		var isErrors = false;

		$('input[required]', $(this).parents('.stage-section')).each(function() {
			if ($(this).val() == '') {
				$(this).addClass('error');
				isErrors = true;
			} else {
				$(this).removeClass('error');
			}
		});

		if (!isErrors) {
			$('input.error').removeClass('error');
			$currentSection = $('.stage-section.current');
			$nextSection = $currentSection.next('.stage-section');
			if ($nextSection.length) {
				$currentSection.hide().removeClass('current');
				$nextSection.show().addClass('current');
			}
		}
	});

	$(document).on('click', '.stage-section .back', function(event) {
		if ($(this).attr('href') == '#') {
			event.preventDefault();
			$currentSection = $('.stage-section.current');
			$previousSection = $currentSection.prev('.stage-section');
			if ($previousSection.length) {
				$currentSection.hide().removeClass('current');
				$previousSection.show().addClass('current');
			}
		}
	});

	// Note Buttons
	$(document).on('click', '.button.note', function(event) {
		event.preventDefault();
		$note = $('<div class="note-overlay" />');
		$noteContent = $(this).data('note') == undefined ? '' : $(this).data('note');
		$note.append('<div class="note-content"><h2>'+$noteContent+'</h2><a href="#" class="button green note-close"><span>Continue</span></a></div>');
		$('body').append($note);
		CenterElement($('.note-content'));
	});

	$(document).on('click', '.button.note-close', function(event) {
		event.preventDefault();
		$('.note-overlay').remove();

		//not stoked about putting this here, but need to remove button toggling from a chapter without refactoring the whole thing
		$('.multiple-choice .button.note').removeClass('green');
	});

	// Provider Notes
	$(window).resize(function() {
		if ($notes = $('.provider-notes')) {
			// $notes.css({
			// 	'bottom' : ($(document).height() - 20) * -1
			// });
		}
	});

	$(window).resize();

	if ($('.provider-notes .notes .note').length == 1) {
		$('.provider-notes').show();
	} else if ($('.provider-notes .notes .note').length > 1) {
		CheckForCardNotes();
	}

	// Swipe Support
	$('.swipable').swipe({
		swipe : swipeSlide,
		threshold : 40,
		allowPageScroll : 'vertical'
	});

	$(".provider-notes .tab").click(function(e){
		e.preventDefault();
		$('.provider-notes').toggleClass("open");
	});

	// $('.provider-notes').swipe({
	// 	swipe : swipeNotes,
	// 	threshold : 40,
	// });

	function swipeSlide(event, direction)
	{
		switch(direction) {
			case 'left' :
				$('.slideshow-controls .next').click();
				CheckForCardNotes();
				break;
			case 'right' :
			$('.slideshow-controls .previous').click();
				CheckForCardNotes();
				break;
		}
	}
	
	$('.slideshow-controls .pick').click(function() {
		CheckForCardNotes();
	});

	function swipeNotes(event, direction)
	{
		switch(direction) {
			case 'up' :
				$('.provider-notes').animate({
					'bottom' : ($(document).height() - $(this).height() + 100) * -1
				});
				break;
			case 'down' :
				$('.provider-notes').animate({
					'bottom' : ($(document).height() - 20) * -1
				});
				break;
		}
	}

	$('form').each(function() {
		if ($('input[type="submit"]', this).length == 0) {
			$submit = $('<input type="submit" />').css({
				'width' : 0,
				'height' : 0,
				'opacity' : 0
			});
			$(this).append($submit);
		}
	});

});

function CheckForCardNotes() {
	$('.provider-notes .notes .note').hide();
	$currentCardId = $('.slides .slide.current').data('cardId');
	$cardNote = $('.provider-notes .note[data-card-id="'+$currentCardId+'"]');
	if ($cardNote.length) {
		$cardNote.show();
		$('.provider-notes').show();
	} else {
		$('.provider-notes').hide();
	}
}

function ContentEdit() {
	$('.button.save').click(function() {
		$("[id^='edit_content']")[0].submit();
	})
}

function ProviderEdit() {
	$('.button.save').click(function() {
		$("[id^='edit_provider']")[0].submit();
	})
}

function ProviderInfo() {
	/* todo bwj blank out state when not US is selected (controller already ignores)
	$('#provider_country_code').change(function() {
		if($(this).val() === "US") {
			//$('.state-code-selectbox').val('');
		} else {
			$('.state-code-selectbox').val('');
		}
	})
	*/
}

function CenterElement(element) {
	$(element).css({
		'position' : 'absolute',
		'left' : '50%',
		'top' : '50%',
		'margin-left' : -($(element).width() / 2),
		'margin-top' : -($(element).height() / 2)
	});
}

// Chapter 1
function Chapter01() {
	$('.buttons .button').draggable({ revert: 'invalid' });

	$('.accept.breasts').droppable({
		accept: '.button.breasts',
		drop: function(event, ui) { Chapter01DropSuccess(ui); }
    });

    $('.accept.arms').droppable({
		accept: '.button.arms',
		drop: function(event, ui) { Chapter01DropSuccess(ui); }
    });

    $('.accept.vagina').droppable({
		accept: '.button.vagina',
		drop: function(event, ui) { Chapter01DropSuccess(ui); }
    });

    $('.accept.legs').droppable({
		accept: '.button.legs',
		drop: function(event, ui) { Chapter01DropSuccess(ui); }
    });

    $('.accept.stomach').droppable({
		accept: '.button.stomach',
		drop: function(event, ui) { Chapter01DropSuccess(ui); }
    });

    $('.accept.head').droppable({
		accept: '.button.head',
		drop: function(event, ui) { Chapter01DropSuccess(ui); }
    });
}

function Chapter01DropSuccess(element, parent) {
	$(element.draggable.context.parentElement).addClass('success');
	element.draggable.addClass('dropped');
	$check = $('.success-check');
	$check.fadeIn(function() {
		setTimeout(function() {
			$check.fadeOut();
		}, 1000);
	});

	if ($('.button.dropped').length == $('.accept').length) {
		$('.button.continue').css({
			'display' : 'inline-block'
		});
	}
}

// Chapter 2
function Chapter02() {
	var speed = $('body').is('.fast') ? 600 : 2000;
	var deflateInterval;

	$('.button.continue').hide;

	$balloonWrap = $('.balloon-top-wrap');

	$(document).on('click', '.button.start', function(event) {
		event.preventDefault();

		$(this).removeClass('start green').addClass('stop red');
		$('span', this).html('Stop');

		/*Deflate($balloonWrap, speed);

		deflateInterval = setInterval(function() {
			Deflate($balloonWrap, speed);
		}, speed * 2);
		*/
		
		DeflateChained($balloonWrap, speed);
		
	});

	$(document).on('click', '.button.stop', function(event) {
		event.preventDefault();

		$('.button.retry').css('display', 'inline-block');
		$('.button.continue').css('display', 'inline-block');

		$(this).removeClass('stop red').addClass('start green');
		$('span', this).html('Start');
		$(this).hide();

		clearInterval(deflateInterval);

	});

	$(document).on('click', '.button.retry', function(event) {
		event.preventDefault();

		clearInterval(deflateInterval);

		$('.button.retry').css('display', 'none');
		$('.button.continue').css('display', 'none');
		$('.button.start').show();
	})
}

function DeflateChained(element, speed) {
	if ($('.button.stop').length > 0) {
		$(element).animate(
			{'width' : '30%'},
			speed,
			function () {
				InflateChained(element, speed);
			}
		);	
	}	
}

function InflateChained(element, speed) {
	$(element).animate(
		{'width' : '100%'},
		speed,
		function () {
			DeflateChained(element, speed);
		}
	);
}

// Chapter 3
function Chapter03() {

	var audioPlayer = $("audio").get(0);

	$(document).on('click', '[data-nav-from]', function(event) {
		event.preventDefault();

		$(".audio-container").fadeIn();

		$('.buttons .button').addClass('inactive');
		$(this).removeClass('inactive');

		var id = $(this).data('navFrom'),
			gender = ($("body").hasClass("boy") ? "male" : "female"),
			audio_url = $(".buttons").data("audio-url");

		audioPlayer.pause();
		audioPlayer.src = audio_url+"pmr/"+id+"_"+gender+".mp3";
		audioPlayer.play();

		$(".button.play").addClass('pause');
		$('.audio.small').addClass('on');

		$("img.body").fadeOut("fast");
		$("img.body[data-nav-to='"+ $(this).data("nav-from") +"']").fadeIn("fast");
	});

	$(".button.play").on("click", function(e){
		e.preventDefault();

		if (audioPlayer.paused) {
			audioPlayer.play();
		} else {
			audioPlayer.pause();
		}

		$(".button.play").toggleClass('pause');
		$('.audio.small').toggleClass('on');
	});
}

// Chapter 4
function Chapter04() {
	$(document).on('click', '.button.check', function(event) {
		event.preventDefault();
		if ($('.selector-bottom a.selected').length) {

			var pathname = window.location.pathname;
			var selected = $('.coin.selected').length;
			var answer = $('.selector-bottom a.selected').data('answer');
			var id = $('.selector-bottom a.selected').data('id');
			var difference = selected - answer;

			$.ajax({
				type: 'PUT',
				url: pathname + '/interaction',
				data: {
					data_key: id,
					data: selected
				}
			});

			if (difference > 0) {
				for (var index = 0; index < difference; index++) {
					$('.coin.selected:first').click();
				}
			} else {
				difference *= -1;
				for (var index = 0; index < difference; index++) {
					$('.coin:not(.selected):first').click();
				}
			}
			$('.answer[data-id="'+id+'"] span').html(answer);
			$('.answer[data-id="'+id+'"]').fadeIn();
			$(this).hide();
		}
	});

	$(document).on('click touchstart', '.selector-bottom a', function(event) {
		event.preventDefault();
		$('body').addClass('coin-operated');
		$('.answers .answer').fadeOut();
		$('.button.check').show();
		$('.coin.selected').click();
	});
}

// Chapter 5
function Chapter05() {
	$(document).ready(function() {
		$('.slide').each(function() {
			CenterElement($('h3', this));
		});
	});
}

// Chapter 6
function Chapter06Writing() {

	var canvas, signaturePad, timer, timerHtml;

	$('.button.draw').addClass('pressed');

	$(document).on('click', '.button.retry', function(event) {
		event.preventDefault();

		$('.canvas').remove();
		$('.stage-bottom').prepend($('<canvas class="canvas" width="756px" height="300px" />'));

		var canvas, signaturePad;

		$button = $(this);

		$startButton = $('.button.start')
		$startButton.removeClass('hide')

		$continueButton = $('.button.continue')
		$continueButton.addClass('hide')

		$button.addClass('hide')

		// 2013-11-25 bwj hack :( see matching pair in 'retry' block above
		$('.righty').css({
			'padding-left' : '8%'
		});
	});

	$(document).on('click', '.button.start', function(event) {
		event.preventDefault();
		if (timer == null || timer.getTime() == 0) {

			$('.button.draw').addClass('pressed');
			$('.button.erase').removeClass('pressed');

			canvas = document.querySelector("canvas");
			signaturePad = new SignaturePad(canvas);

			$button = $(this);
			$button.removeClass('green');
			$(canvas).css({ 'background' : 'none' });

			timerHtml = '<em class="timer"><b class="minute">00</b>:<b class="second">00</b></em>';

			$('.button.start span').html(timerHtml);

			timer = new _timer
		    (
		        function(time)
		        {
		        	if(time == 10)
		            {
		            	$button.addClass('red');
		            }
		            else if(time == 0)
		            {
		                timer.stop();
						$button.removeClass('red');
						$button.addClass('green')
						$button.html('<span>Start</span>')

						$retryButton = $('.button.retry')
						$retryButton.removeClass('hide')

						$continueButton = $('.button.continue')
						$continueButton.removeClass('hide')

						$button.addClass('hide')

						// 2013-11-25 bwj hack :( see matching pair in 'retry' block above
						$('.righty').css({
							'padding-left' : '0%'
						});
		            }
		        }
		    )

		    timer.reset(90);
		    timer.mode(0);
		    timer.start(1000);
		}

	});

	$(document).on('click', '.button.draw', function(event) {
		event.preventDefault();
		if ($(this).is('.pressed')) {
			return;
		}
		$(this).toggleClass('pressed');
		$('.button.erase').toggleClass('pressed');
		signaturePad.maxWidth = 2.5;
		signaturePad.minWidth = .5;
		signaturePad.penColor = 'black';
	});

	$(document).on('click', '.button.erase', function(event) {
		event.preventDefault();
		if ($(this).is('.pressed')) {
			return;
		}
		$(this).toggleClass('pressed');
		$('.button.draw').toggleClass('pressed');
		signaturePad.maxWidth = 10;
		signaturePad.minWidth = 10;
		signaturePad.penColor = 'white';
	});

}

function ShuffleArray(array) {

	var currentIndex = array.length, temporaryValue, randomIndex;

	while (0 !== currentIndex) {
		randomIndex = Math.floor(Math.random() * currentIndex);
  		currentIndex -= 1;

  		temporaryValue = array[currentIndex];
  		array[currentIndex] = array[randomIndex];
  		array[randomIndex] = temporaryValue;
  	}

  	return array;
}

function RandomizeSlides(spinning, selecteds) {
	if (!spinning && (spins < $('.slide').length)) {
		$('body').removeClass('selected');
		spinning = true;
		spins++;
		var delay = 0;
		for (var index = 0; index < 8; index++) {

			setTimeout(function() {
				var randoms = Array();
				$('.slide span.was-selected').each(function() {
					selecteds.push($(this).parents('.slide').html());
					$(this).parents('.slide').html('');
				});

				$('.slide span:not(.was-selected)').each(function() {
					randoms.push($(this).parents('.slide').html());
					$(this).parents('.slide').html('');
				});

				ShuffleArray(randoms);
				ShuffleArray(selecteds);

				$('.slide:eq(2)').html(randoms.pop());

				$(randoms).each(function(i) {
					$('.slide').each(function() {
						if ($(this).html() == '') {
							$(this).html(randoms.pop());
						}
					});
				});

				$(selecteds).each(function(i) {
					$('.slide').each(function() {
						if ($(this).html() == '') {
							$(this).html(selecteds.pop());
						}
					});
				});
			}, delay);
			delay += 100;
		}
		setTimeout(function() {
			$('.slide:eq(2), body').addClass('selected');
			$('.slide:eq(2) span').addClass('was-selected');
		}, 600, function() {
			spinning = false;
		});
	}
}

function Chapter06Charades() {

	var spinning = false;
	var selecteds = Array();

	$('.spinner').swipe({
		swipe : swipeSpin,
		threshold : 40
	});

	function swipeSpin(event, direction)
	{
		switch(direction) {
			case 'up' :
				RandomizeSlides(spinning, selecteds);
				break;
			case 'down' :
				RandomizeSlides(spinning, selecteds);
				break;
		}
	}
	
	$('.charades li a.button').click(function(){
		$('.charades li a.button').each(function(){
			if ($(this).hasClass('green')) {
				$(this).removeClass('green');
				$(this).addClass($(this).attr('data-color'));
			}
		})
		$(this).removeClass($(this).attr('data-color'));
		$(this).addClass('green');
	});

}

// Chapter 7
function Chapter07() {

	$('.matching .buttons .button').draggable({ revert: 'invalid' });

	$('.accept.thoughts').droppable({
		accept: '.button.do, .button.smart, .button.madatme, .button.myfault',
		drop: function(event, ui) { Chapter07DropSuccess(ui); }
    });

    $('.accept.feelings').droppable({
		accept: '.button.mad, .button.happy, .button.sad, .button.scared',
		drop: function(event, ui) { Chapter07DropSuccess(ui); }
    });

    $('.accept.actions').droppable({
		accept: '.button.crying, .button.breath, .button.hitting, .button.talking',
		drop: function(event, ui) { Chapter07DropSuccess(ui); }
    });

    $(".selector .selector-bottom ul li").on("click touchstart",function(){
    	var trackID = $(this).find("a .right").data('track');
    	var trackTitle = $(this).find("a .right").text().replace(".","");
     	$.cookie("chapter07_trackID", trackID, {expires: 1});
     	$.cookie("chapter07_trackTitle", trackTitle, {expires: 1});
    	$(".button.continue").css({display:"inline-block"});
    });

	$(document).on('click', '.multiple-choice .buttons .button', function(event) {
		event.preventDefault();
		$(this).toggleClass('green');
		if ($(this).hasClass('green')) {
			$('.multiple-choice .button.continue.hidden').css({
				'display' : 'inline-block'
			});
		}
	});
}

function Chapter07DropSuccess(element, parent) {
	$(element.draggable.context.parentElement).addClass('success');
	element.draggable.addClass('dropped');
	$check = $('.success-check');
	$check.fadeIn(function() {
		setTimeout(function() {
			$check.fadeOut();
		}, 1000);
	});

	if ($('.button.dropped').length == $('.accept').length * 2) {
		$('.button.continue').css({
			'display' : 'inline-block'
		});
	}
}

// Chapter 8
function Chapter08() {}

// Chapter 9
function Chapter09ListExercise() {

	$(document).on('click touchstart', '.list-items h3', function(event) {
		event.preventDefault();
		//bwj removing this interaction $(this).toggleClass('crossed');
	});
}

function Chapter09LadderExercise() {
	$('.ladder-items').sortable({ items: '> .ladder-item' });
}

function Chapter11Videos() {
	$(".video-overlay").on("click", function(e){
		e.preventDefault();

		var $slide = $(this).closest('.slide');

		if ($slide.hasClass("current")) {
			var $iframe = $(this).closest('.slide').find("iframe"),
				player = $f( $iframe.get(0) );

			if ($iframe.attr("data-playing")){
				$iframe.removeAttr("data-playing")
				player.api("pause");
			} else {
				$iframe.attr("data-playing","true");
				player.api("play");
			}
		}
	})
}



// Audio
function Audio() {

	var audioElement = document.createElement('audio');
	audioElement.setAttribute('src', $('.audio-file').data('source'));
	audioElement.addEventListener('ended', function(e) {
		$('.button.play').toggleClass('pause');
		$('.audio.small').toggleClass('on');
	});

	$(document).on('click', '.button.play', function(event) {
		event.preventDefault();
		if ($(this).is('.pause')) {
			audioElement.pause();
		} else {
			audioElement.play();
		}
		$(this).toggleClass('pause');
		$('.audio.small').toggleClass('on');
	});
}

// Password Recovery
function PasswordRecovery() {

	$(document).on('click', '.button.submit', function(event) {
		event.preventDefault();
		$('.password-recovery-form').fadeOut(function() {
			$('.recovery-success').fadeIn();
		});
	});
}

// Patient Homework
function PatientHomework() {

	var chapterId = $('.stage-bottom').data('chapterId');
	$('.button.chapter-'+chapterId).addClass('green').click();

	$(document).on('click touchstart', '.stage-bottom .middle .button', function() {
		$('#categoryContainer .button').removeClass("green");
		$(this).addClass('green');
	})

}

// New Patient
function NewPatient() {

	$(document).on('click', '.stage-section .continue', function(event) {
		$('span.lookup-id').html($('input[name="patient[lookup_id]"]').val());
	});
}

// Provider Home
function ProviderHome() {

	$(document).on('change', '.provider-home select', function() {
		if ($(':selected', this).val() == '') {
			$('.button.prepare').css({
				'display' : 'none'
			});
		} else {
			window.location = GetCurrentBaseUrl() + $(':selected', this).data('url')
		}
	});
}

function EditDeck() {
	$('.cards').sortable({
		items: '.input-row',
		handle: '.handle',
		update: function (event, ui) {
			$(".cards .input-row").each(function(index){
				$(this).find("input.order").val(index);
			});
		}
	});
}

function GetCurrentBaseUrl() {
	var port = '';
	if (window.location.port != '80') {
		port = ':'+ window.location.port;
	}

	var url = window.location.protocol + '//' + window.location.hostname + port;
	return url;
}


// FROOGALOOP - VIMEO API
var Froogaloop=function(){function e(a){return new e.fn.init(a)}function h(a,c,b){if(!b.contentWindow.postMessage)return!1;var f=b.getAttribute("src").split("?")[0],a=JSON.stringify({method:a,value:c});"//"===f.substr(0,2)&&(f=window.location.protocol+f);b.contentWindow.postMessage(a,f)}function j(a){var c,b;try{c=JSON.parse(a.data),b=c.event||c.method}catch(f){}"ready"==b&&!i&&(i=!0);if(a.origin!=k)return!1;var a=c.value,e=c.data,g=""===g?null:c.player_id;c=g?d[g][b]:d[b];b=[];if(!c)return!1;void 0!==
a&&b.push(a);e&&b.push(e);g&&b.push(g);return 0<b.length?c.apply(null,b):c.call()}function l(a,c,b){b?(d[b]||(d[b]={}),d[b][a]=c):d[a]=c}var d={},i=!1,k="";e.fn=e.prototype={element:null,init:function(a){"string"===typeof a&&(a=document.getElementById(a));this.element=a;a=this.element.getAttribute("src");"//"===a.substr(0,2)&&(a=window.location.protocol+a);for(var a=a.split("/"),c="",b=0,f=a.length;b<f;b++){if(3>b)c+=a[b];else break;2>b&&(c+="/")}k=c;return this},api:function(a,c){if(!this.element||
!a)return!1;var b=this.element,f=""!==b.id?b.id:null,d=!c||!c.constructor||!c.call||!c.apply?c:null,e=c&&c.constructor&&c.call&&c.apply?c:null;e&&l(a,e,f);h(a,d,b);return this},addEvent:function(a,c){if(!this.element)return!1;var b=this.element,d=""!==b.id?b.id:null;l(a,c,d);"ready"!=a?h("addEventListener",a,b):"ready"==a&&i&&c.call(null,d);return this},removeEvent:function(a){if(!this.element)return!1;var c=this.element,b;a:{if((b=""!==c.id?c.id:null)&&d[b]){if(!d[b][a]){b=!1;break a}d[b][a]=null}else{if(!d[a]){b=
!1;break a}d[a]=null}b=!0}"ready"!=a&&b&&h("removeEventListener",a,c)}};e.fn.init.prototype=e.fn;window.addEventListener?window.addEventListener("message",j,!1):window.attachEvent("onmessage",j);return window.Froogaloop=window.$f=e}();

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function(e){if(typeof define==="function"&&define.amd){define(["jquery"],e)}else if(typeof exports==="object"){e(require("jquery"))}else{e(jQuery)}})(function(e){function n(e){return u.raw?e:encodeURIComponent(e)}function r(e){return u.raw?e:decodeURIComponent(e)}function i(e){return n(u.json?JSON.stringify(e):String(e))}function s(e){if(e.indexOf('"')===0){e=e.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\")}try{e=decodeURIComponent(e.replace(t," "));return u.json?JSON.parse(e):e}catch(n){}}function o(t,n){var r=u.raw?t:s(t);return e.isFunction(n)?n(r):r}var t=/\+/g;var u=e.cookie=function(t,s,a){if(arguments.length>1&&!e.isFunction(s)){a=e.extend({},u.defaults,a);if(typeof a.expires==="number"){var f=a.expires,l=a.expires=new Date;l.setTime(+l+f*864e5)}return document.cookie=[n(t),"=",i(s),a.expires?"; expires="+a.expires.toUTCString():"",a.path?"; path="+a.path:"",a.domain?"; domain="+a.domain:"",a.secure?"; secure":""].join("")}var c=t?undefined:{};var h=document.cookie?document.cookie.split("; "):[];for(var p=0,d=h.length;p<d;p++){var v=h[p].split("=");var m=r(v.shift());var g=v.join("=");if(t&&t===m){c=o(g,s);break}if(!t&&(g=o(g))!==undefined){c[m]=g}}return c};u.defaults={};e.removeCookie=function(t,n){if(e.cookie(t)===undefined){return false}e.cookie(t,"",e.extend({},n,{expires:-1}));return!e.cookie(t)}});
