class SectionQuestion < ActiveRecord::Base
  belongs_to :section
  
  validates :question, presence: true
  validates :answer_boys, presence: true
  validates :answer_girls, presence: true
  
  def answer_boys_reduced()
    return ((answer_boys / 100.0) * 20).ceil
  end
  
  def answer_girls_reduced()
    return ((answer_girls / 100.0) * 20).ceil
  end
  
end
