class Assignment < ActiveRecord::Base
  validates :homework_id, presence: true, uniqueness: { scope: :patient_id }
  validates :patient_id, presence: true

  belongs_to :homework
  belongs_to :patient
  
end
