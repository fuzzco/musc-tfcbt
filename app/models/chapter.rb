class Chapter < ActiveRecord::Base
  validates :name, presence: true
  validates :display_order, presence: true

  has_many :sections

  def display_name
    return "#{display_order} - #{name}"
  end

  def first_vimeo_url
    sections.first.vimeo_url
  end

end
