class Note < ActiveRecord::Base
	validates :content, presence: true
  
	belongs_to :section
	belongs_to :card
end
