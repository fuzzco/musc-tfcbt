class Profession < ActiveRecord::Base
  validates :description, presence: true
  
  has_many :providers
  
end
