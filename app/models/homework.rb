class Homework < ActiveRecord::Base
  validates   :description, presence: true
  
  belongs_to  :homework_category
  
  has_many    :assignments
  has_many    :patients, through: :assignments

end
