class Section < ActiveRecord::Base
  validates :chapter_id, presence: true
  validates :sequence, uniqueness: { scope: :chapter_id }
  validates :sequence, presence: true
  validates :content_type, inclusion: { in: %w(video), allow_nil: true }

  belongs_to :chapter
  
  has_many :interactions
  has_many :notes
  has_many :decks
  has_many :section_questions

end
