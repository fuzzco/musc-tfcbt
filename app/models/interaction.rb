class Interaction < ActiveRecord::Base
  validates :description, presence: true, inclusion: { in: %w(Visit Response) }
  validates :patient_id, presence: true
  validates :section_id, presence: true
  
  belongs_to :patient
  belongs_to :section
  
end
