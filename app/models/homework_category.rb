class HomeworkCategory < ActiveRecord::Base
  validates   :description, presence: true
  
  has_many    :homeworks
  
end
