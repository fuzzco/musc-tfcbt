class Deck < ActiveRecord::Base
  validates :title, presence: true

  belongs_to :provider  
  belongs_to :section
  has_many :cards, :order => 'card_order ASC'

  accepts_nested_attributes_for :cards
  
end
