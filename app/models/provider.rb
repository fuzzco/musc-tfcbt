class Provider < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # bwj removed :rememberable, :registerable  until we get there
  devise :database_authenticatable, :trackable, :validatable, :timeoutable, :recoverable
         
  validates   :name, presence: true, length: { maximum: 100 }
  validates   :email, presence: true, uniqueness: {case_sensitive: false }
  validates   :password, presence: true, length: { minimum: 8 }, on: :create
  validates   :password, length: { minimum: 8 }, on: :update, allow_blank: true
  
  belongs_to  :profession
  belongs_to  :degree
  belongs_to  :experience
  
  has_many    :patients, :dependent => :destroy
  has_many    :decks, :dependent => :destroy
  
  before_save { self.email = email.downcase }
  
  after_create :set_decks
    def set_decks
      provider_id = self.id
      Deck.where(original: true).each do |deck|
        deck_copy = deck.deep_clone :include => {:cards => :notes}
        deck_copy.provider_id = provider_id
        deck_copy.original = false
        deck_copy.save
      end
    end
end
