class Card < ActiveRecord::Base  
  belongs_to :deck
  has_many :notes

  after_initialize :init

    def init
      self.card_order  ||= self.id
    end
end
