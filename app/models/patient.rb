class Patient < ActiveRecord::Base
  validates   :lookup_id, presence: true
  validates   :provider_id, presence: true
  validates   :age, presence: true
  validates   :race_id, presence: true
  validates   :ethnicity_id, presence: true
  validates   :gender_id, presence: true
  validates   :trauma1_id, presence: true
  
  belongs_to  :race
  belongs_to  :ethnicity
  belongs_to  :gender
  belongs_to  :provider
  
  belongs_to :trauma1, :class_name => "Trauma", :foreign_key => "trauma1_id"
  belongs_to :trauma2, :class_name => "Trauma", :foreign_key => "trauma2_id"
  belongs_to :trauma3, :class_name => "Trauma", :foreign_key => "trauma3_id"
  belongs_to :trauma4, :class_name => "Trauma", :foreign_key => "trauma4_id"
  
  has_many    :interactions, :dependent => :destroy
  has_many    :assignments, :dependent => :destroy
  has_many    :homeworks, through: :assignments
  
  before_create :default_values
  
  private
    def default_values
      is_active ||= true
    end
end
